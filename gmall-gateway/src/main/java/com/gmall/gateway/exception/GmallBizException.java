package com.gmall.gateway.exception;

import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/7/2 17:31
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class GmallBizException extends Exception{
    /**
     * error code
     */
    private int code;

    /**
     * error message
     */
    private String message;

    public GmallBizException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
