package com.gmall.gateway.handle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author HL.Wu
 * @date 2020/7/2 17:41
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Component
@Slf4j
public class SignValidateHandler {

    public JSONObject handle(JSONObject paramObj) {
        log.info("current request param info is : \n {}", JSON.toJSONString(paramObj));
        //TODO 签名校验相关业务
        return null;
    }
}
