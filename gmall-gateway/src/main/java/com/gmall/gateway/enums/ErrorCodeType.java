package com.gmall.gateway.enums;

import lombok.Getter;

/**
 * @author HL.Wu
 * @date 2020/7/2 17:53
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Getter
public enum ErrorCodeType {
    SUCCESS(0, "成功")
    ;
    private int code;

    private String desc;

    ErrorCodeType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
