package com.gmall.gateway.model;

import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/7/2 17:28
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class ResponseModel {

    /**
     * 返回错误码
     */
    private int code;

    /**
     * 返回展示用户信息
     */
    private String message;

    /**
     * 返回参数信息
     */
    private Object data;

    /**
     * 开发者信息
     */
    private String info;
}
