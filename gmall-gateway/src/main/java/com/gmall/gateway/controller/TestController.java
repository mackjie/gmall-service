package com.gmall.gateway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HL.Wu
 * @date 2020/7/2 17:58
 * Copyright ©www.sioo.cn Copyright@2009-2020 AII Right Reserve
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/exception")
    private void test(){
        int i = 1/0;
    }
}
