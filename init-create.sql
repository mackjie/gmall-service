DROP DATABASE IF EXISTS gmall;
CREATE DATABASE IF NOT EXISTS gmall DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

-- 用户表
create table t_user(
       `id` varchar(50) primary key comment '唯一标识',
       `user_name` varchar(50) not null comment '用户名称',
       `nick_name` varchar(50) default null comment '用户昵称',
       `password` varchar(255) not null comment '登录密码',
       `type` tinyint(1) default null comment '类型 0：个人；1：公司',
       `status` tinyint(1) default 0 comment '状态 0：正常；1：禁用',
       `parent_id` varchar(50) default null comment '父id',
       `user_info_id` varchar(50) default null comment '个人类型：用户详情表id',
       `company_id` varchar(50) default null comment '公司类型：公司详情表id',
       `wechat_unionid` varchar(50) default null comment '绑定微信关联id',
       `invite_code` varchar(20) default null comment '邀请码',
       `invite_cnt` integer default 0 comment '邀请数量',
       `pay_password` varchar(255) default null comment '支付密码',
       `expire_date` datetime  default null comment '过期时间',
       `is_deleted` tinyint(1)  default 0 comment '删除 0：正常；1：删除',
       `creator` varchar(50)  default null comment '创建者',
       `updater` varchar(50)  default null comment '更新者',
       `create_date` datetime comment '创建时间',
       `update_date` datetime comment '更新时间'
);

alter table t_user add column sex int DEFAULT 0 COMMENT '0、未知，1、男，2、女';
alter table t_user add column tags_view int DEFAULT 0 COMMENT '0：展示；1：隐藏';
alter table t_user add column importance int DEFAULT 0 COMMENT '重要性等级';
alter table t_user add column header_logo_view int DEFAULT 0 COMMENT '0：展示；1：隐藏';
alter table t_user add index inx_user(`user_name`,`type`,`parent_id`,`status`);

-- 公司信息表
create table t_company(
      `id` varchar(50) primary key comment '唯一标识',
      `name` varchar(255) not null comment '用户名称',
      `mobile` varchar(20) default null comment '公司座机',
      `address` varchar(255) default null comment '公司地址',
      `logo` varchar(255) default null comment '公司logo，也会作为公司头像使用',
      `category` integer default null comment '行业类别',
      `contacts` varchar(50) default null comment '联系人姓名',
      `legal_person` varchar(50) default null comment '公司法人',
      `legal_card` varchar(18) default null comment '法人身份证号',
      `phone` varchar(11) default null comment '手机号码',
      `email` varchar(50) default null comment '公司邮箱',
      `stock_code` varchar(50) default null comment '证券代码',
      `description` mediumtext default null comment '公司简介',
      `creator` varchar(50)  default null comment '创建者',
      `updater` varchar(50)  default null comment '更新者',
      `create_date` datetime comment '创建时间',
      `update_date` datetime comment '更新时间'
);
alter table t_company add index inx_company(`name`,`category`);

-- 用户详情表
create table t_user_info(
        `id` varchar(50) primary key comment '唯一标识',
        `avatar` varchar(255) default null comment '头像',
        `phone` varchar(20) default null comment '手机号码',
        `contacts` varchar(50) default null comment '联系人姓名',
        `email` varchar(50) default null comment '个人邮箱',
        `address` varchar(255) default null comment '地址',
        `category` integer default null comment '关注类别',
        `creator` varchar(50)  default null comment '创建者',
        `updater` varchar(50)  default null comment '更新者',
        `create_date` datetime comment '创建时间',
        `update_date` datetime comment '更新时间'
);

-- 角色表
create table sys_role(
     `id` int primary key auto_increment comment '唯一标识',
     `name` varchar(50) not null comment '角色名称',
     `code` varchar(50) default null comment '角色code',
     `comment` varchar(50) default null comment '备注',
     `is_deleted` tinyint(1) default 0 comment '删除 0：正常；1：删除',
     `creator` varchar(50)  default null comment '创建者',
     `updater` varchar(50)  default null comment '更新者',
     `create_date` datetime comment '创建时间',
     `update_date` datetime comment '更新时间'
);
alter table sys_role add index inx_role(`name`);
alter table sys_role add COLUMN company_id varchar(50) COMMENT '所属公司id';

-- 资源表
create table sys_resource(
         `id` int primary key auto_increment comment '唯一标识',
         `name` varchar(50) not null comment '资源名称',
         `code` varchar(50) default null comment '资源code',
         `type` tinyint(1) default null comment '类型 0；目录 1：菜单 2：按钮',
         `parent_id` integer default 0 comment '父id',
         `platform_type` tinyint(1) default null comment '平台类型 0：公司类型 1：个人类型',
         `request_path` varchar(255) default null comment '请求路径',
         `icon` varchar(255) default null comment '图标',
         `sort_no` integer default null comment '排序序号',
         `comment` varchar(255) default null comment '备注',
         `is_deleted` tinyint(1) default 0 comment '删除 0：正常；1：删除',
         `creator` varchar(50)  default null comment '创建者',
         `updater` varchar(50)  default null comment '更新者',
         `create_date` datetime comment '创建时间',
         `update_date` datetime comment '更新时间'
);

alter table sys_resource add index inx_resource(`name`,`parent_id`);
alter table sys_resource add COLUMN company_id varchar(50) COMMENT '所属公司id';

-- 用户角色关联表
create table sys_user_role(
      `id` int primary key auto_increment comment '唯一标识',
      `user_id` varchar(50) not null comment '用户id',
      `role_id` int not null comment '角色id',
      `creator` varchar(50)  default null comment '创建者',
      `updater` varchar(50)  default null comment '更新者',
      `create_date` datetime comment '创建时间',
      `update_date` datetime comment '更新时间'
);
alter table sys_user_role add index inx_user_role(`user_id`,`role_id`);


-- 角色资源表
create table sys_role_resource(
      `id` int primary key auto_increment comment '唯一标识',
      `role_id` int not null comment '角色id',
      `resource_id` int not null comment '资源id',
      `creator` varchar(50)  default null comment '创建者',
      `updater` varchar(50)  default null comment '更新者',
      `create_date` datetime comment '创建时间',
      `update_date` datetime comment '更新时间'
);
alter table sys_role_resource add index inx_role_resource(`resource_id`,`role_id`);

-- 部门表
create table t_dept(
	`id` bigint primary key auto_increment comment '唯一标识',
	`dept_name` varchar(255) not null comment '部门名称',
	`dept_fullname` varchar(255) default null comment '部门全称',
	`order_no` int default null comment '排序',
	`company_id` bigint default null comment '所属公司id',
	`comment` varchar(255) default null comment '备注',
	`parent_id` bigint default null comment '父部门id',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table t_dept add index inx_dept(`dept_name`,`company_id`,`parent_id`);


-- 用户-部门关联表
create table t_user_dept(
	`id` bigint primary key auto_increment comment '唯一标识',
	`user_id` varchar(50) default null comment '用户id',
	`dept_id` bigint default null comment '部门id',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table t_user_dept add index inx_user_dept(`user_id`,`dept_id`);


-- 职位表
create table t_position(
	`id` bigint primary key auto_increment comment '唯一标识',
	`position_name` varchar(50) default null comment '职位名称',
	`position_code` varchar(50) default null comment '职位code',
	`company_id` varchar(255) default null comment '所属公司id',
	`comment` varchar(255) default null comment '备注',
	`status` integer default null comment '状态',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table t_position add index inx_position(`company_id`,`position_name`);

-- 用户-职位关联表
create table t_user_position(
	`id` bigint primary key auto_increment comment '唯一标识',
	`user_id` varchar(50) default null comment '用户id',
	`position_id` bigint default null comment '职位id',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table t_user_position add index inx_user_position(`user_id`,`position_id`);

-- 系统变量表
create table sys_config(
	`id` bigint primary key auto_increment comment '唯一标识',
	`config_name` varchar(255) default null comment '配置名称',
	`config_code` varchar(255) default null comment '配置变量code',
	`comment` varchar(255) default null comment '备注',
	`status` integer default null comment '状态',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table sys_config add index inx_config(`config_name`,`config_code`);

-- 用户站内信信息关联表
create table sys_msg_log(
	`id` bigint primary key auto_increment comment '唯一标识',
	`user_id` varchar(255) default null comment '用户id',
	`template_id` varchar(255) default null comment '模板id',
	`status` integer default 0 comment '状态（0：未读；1：已读）',
	`is_deleted` tinyint(1) default 0 comment '删除 0：正常；1：删除',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table sys_msg_log add index inx_msg_log(`user_id`,`template_id`,`status`);

-- 站内信模板表
create table sys_msg_template(
	`id` bigint primary key auto_increment comment '唯一标识',
	`status` integer default 0 comment '状态（0：正常；1：禁用）',
	`type` varchar(50) default null comment '消息类型，msg_notice:通知类型；msg_event:事件类型；msg_alarm:告警类型；msg_remind:提醒类型',
	`channel` varchar(50) default null comment '发送渠道，sys_msg:站内信；email:邮件；sms:短信；wechat:微信；enterprise_wechat:企业微信',
	`title` varchar(100) default null comment '消息标题',
	`content` mediumtext default null comment '消息内容',
	`msg_from` varchar(50) default null comment '消息来源，platform:平台；devops:运维；channel:通道',
	`receiver_type` int default null comment '接收者类型 0：个人；1：公司；2：所有',
	`comment` varchar(255) default null comment '备注',
	`creator` varchar(50)  default null comment '创建者',
	`updater` varchar(50)  default null comment '更新者',
	`create_date` datetime comment '创建时间',
	`update_date` datetime comment '更新时间'
);
alter table sys_msg_template add index inx_msg_template(`status`,`type`);




