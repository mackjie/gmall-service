package com.gmall.user.server.restapi;

import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.RoleQueryForm;
import com.gmall.user.api.po.Role;
import com.gmall.user.server.biz.RoleBiz;
import com.gmall.user.server.service.RoleService;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author HL.Wu
 * @date 2020/6/9 13:50
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@RestController
@Slf4j
@RequestMapping("/role")
public class RoleApi {

    @Autowired
    private RoleBiz roleBiz;
    @Autowired
    private RoleService roleService;

    /**
     * 查询用户所拥有的角色信息
     *
     * @return
     */
    @PostMapping("/list")
    public Response<ResultList<Role>> getRoleList(@RequestBody RoleQueryForm roleQueryForm){
        return roleBiz.getUserRole(roleQueryForm);
    }

    @PostMapping("/save")
    public int save(@RequestBody Role role){
        return roleService.save(role);
    }

    @PostMapping("/update")
    public int update(@RequestBody Role role){
        return roleService.update(role);
    }

    @GetMapping("/delete")
    public int delete(@RequestParam("id") @NotNull Integer id){
        return roleService.deleteById(id);
    }
}
