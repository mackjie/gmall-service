package com.gmall.user.server.service;

import com.gmall.user.api.form.RoleQueryForm;
import com.gmall.user.api.po.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:24
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public interface RoleService {
    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(Integer id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(Role record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    Role findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(Role record);

    /**
     * 条件查询
     *
     * @param roleQueryForm
     * @return
     */
    List<Role> list(RoleQueryForm roleQueryForm);

    /**
     * 条件数量统计
     *
     * @param roleQueryForm
     * @return
     */
    Integer count(RoleQueryForm roleQueryForm);

    /**
     * 唯一 条件查询 详情
     *
     * @param roleQueryForm
     * @return
     */
    Role findByForm(RoleQueryForm roleQueryForm);

    /**
     *  查询用户所有角色信息
     *
     * @param userId
     * @return
     */
    List<Role> getRoleByUserId(String userId);
}
