package com.gmall.user.server.mapper;

import com.gmall.user.api.po.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface DeptMapper {

    int deleteById(Long id);

    int save(Dept record);

    Dept findById(Long id);

    int update(Dept record);
}