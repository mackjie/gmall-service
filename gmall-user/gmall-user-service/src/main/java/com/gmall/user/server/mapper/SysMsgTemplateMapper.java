package com.gmall.user.server.mapper;

import com.gmall.user.api.po.SysMsgTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SysMsgTemplateMapper {
    int deleteById(Long id);

    int save(SysMsgTemplate record);

    SysMsgTemplate findById(Long id);

    int update(SysMsgTemplate record);
}