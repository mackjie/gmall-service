package com.gmall.user.server.mapper;

import com.gmall.user.api.po.SysConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SysConfigMapper {
    int deleteById(Long id);

    int save(SysConfig record);

    SysConfig findById(Long id);

    int update(SysConfig record);
}