package com.gmall.user.server.service;

import com.gmall.user.api.form.UserInfoQueryForm;
import com.gmall.user.api.po.UserInfo;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:25
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public interface UserInfoService {
    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(String id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(UserInfo record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    UserInfo findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(UserInfo record);

    /**
     * 唯一 条件查询 详情
     *
     * @param userInfoQueryForm
     * @return
     */
    UserInfo findByForm(UserInfoQueryForm userInfoQueryForm);
}
