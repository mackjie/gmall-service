package com.gmall.user.server.service.impl;

import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.vo.ResourceVo;
import com.gmall.user.server.mapper.ResourceMapper;
import com.gmall.user.server.service.ResourceService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:27
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    private final ResourceMapper resourceMapper;

    public ResourceServiceImpl(ResourceMapper resourceMapper) {
        this.resourceMapper = resourceMapper;
    }

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    @Override
    public int deleteById(Integer id) {
        return resourceMapper.deleteById(id);
    }

    /**
     * 添加
     *
     * @param record
     * @return
     */
    @Override
    public int save(Resource record) {
        return resourceMapper.save(record);
    }

    /**
     * 查询
     *
     * @param id
     * @return
     */
    @Override
    public Resource findById(String id) {
        return resourceMapper.findById(id);
    }

    /**
     * 更新
     *
     * @param record
     * @return
     */
    @Override
    public int update(Resource record) {
        return resourceMapper.update(record);
    }

    /**
     * 唯一 条件查询 详情
     *
     * @param resourceQueryForm
     * @return
     */
    @Override
    public Resource findByForm(ResourceQueryForm resourceQueryForm) {
        return resourceMapper.findByForm(resourceQueryForm);
    }

    /**
     * 根据角色id 查询该角色下全部（目录、菜单）权限
     *
     * @param userId
     * @return
     */
    @Override
    public List<ResourceVo> getResourceByUserId(String userId, Integer type, Integer parentId) {
        return resourceMapper.getResourceByUserId(userId,type,parentId);
    }

    /**
     * 条件查询资源 数量
     *
     * @param resourceQueryForm
     * @return
     */
    @Override
    public int count(ResourceQueryForm resourceQueryForm) {
        return resourceMapper.count(resourceQueryForm);
    }

    /**
     * 条件查询资源
     *
     * @param resourceQueryForm
     * @return
     */
    @Override
    public List<Resource> list(ResourceQueryForm resourceQueryForm) {
        return resourceMapper.list(resourceQueryForm);
    }
}
