package com.gmall.user.server.mapper;

import com.gmall.user.api.form.CompanyQueryForm;
import com.gmall.user.api.po.Company;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CompanyMapper {

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(String id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(Company record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    Company findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(Company record);

    /**
     * 唯一 条件查询 详情
     *
     * @param companyQueryForm
     * @return
     */
    Company findByForm(CompanyQueryForm companyQueryForm);
}