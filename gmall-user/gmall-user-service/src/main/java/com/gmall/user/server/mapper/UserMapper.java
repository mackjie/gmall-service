package com.gmall.user.server.mapper;

import com.gmall.user.api.form.UserQueryForm;
import com.gmall.user.api.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(String id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(User record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    User findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(User record);

    /**
     * 条件查询
     *
     * @param userQueryForm
     * @return
     */
    List<User> list(UserQueryForm userQueryForm);

    /**
     * 条件数量统计
     *
     * @param userQueryForm
     * @return
     */
    Integer count(UserQueryForm userQueryForm);

    /**
     * 唯一 条件查询 详情
     *
     * @param userQueryForm
     * @return
     */
    User findByForm(UserQueryForm userQueryForm);
}