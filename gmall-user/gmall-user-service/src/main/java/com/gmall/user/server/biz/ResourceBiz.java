package com.gmall.user.server.biz;

import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.server.service.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/6/9 18:42
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Component
@Slf4j
public class ResourceBiz {

    @Autowired
    private ResourceService resourceService;

    /**
     * 根据公司名称查询
     * 如果为
     * 公司 查询该公司所有权限
     *
     * @return
     */
    public Response<ResultList<Resource>> list(ResourceQueryForm resourceQueryForm){
        Response response = Response.newInstance();
        try {
            ResultList<Resource> result = ResultList.emptyList();
            // 根据公司id 查询所属公司所有权限列表
            int cnt = resourceService.count(resourceQueryForm);
            List<Resource> list = resourceService.list(resourceQueryForm);
            result.setTotal(cnt);
            result.setResult(list);
            result.setPageSize(resourceQueryForm.getPageSize());
            result.setPageNum(resourceQueryForm.getPageNum());
            response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText(),result);
        } catch (Exception e) {
            response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        }
        return response;
    }
}
