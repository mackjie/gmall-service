package com.gmall.user.server.service.impl;

import com.gmall.user.api.form.RoleQueryForm;
import com.gmall.user.api.po.Role;
import com.gmall.user.server.mapper.RoleMapper;
import com.gmall.user.server.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:26
 * Copyright ©www.sioo.cn Copyright AII Right Reserve
 */
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleMapper roleMapper;

    public RoleServiceImpl(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    @Override
    public int deleteById(Integer id) {
        return roleMapper.deleteById(id);
    }

    /**
     * 添加
     *
     * @param record
     * @return
     */
    @Override
    public int save(Role record) {
        return roleMapper.save(record);
    }

    /**
     * 查询
     *
     * @param id
     * @return
     */
    @Override
    public Role findById(String id) {
        return roleMapper.findById(id);
    }

    /**
     * 更新
     *
     * @param record
     * @return
     */
    @Override
    public int update(Role record) {
        return roleMapper.update(record);
    }

    /**
     * 条件查询
     *
     * @param roleQueryForm
     * @return
     */
    @Override
    public List<Role> list(RoleQueryForm roleQueryForm) {
        return roleMapper.list(roleQueryForm);
    }

    /**
     * 条件数量统计
     *
     * @param roleQueryForm
     * @return
     */
    @Override
    public Integer count(RoleQueryForm roleQueryForm) {
        return roleMapper.count(roleQueryForm);
    }

    /**
     * 唯一 条件查询 详情
     *
     * @param roleQueryForm
     * @return
     */
    @Override
    public Role findByForm(RoleQueryForm roleQueryForm) {
        return roleMapper.findByForm(roleQueryForm);
    }

    /**
     * 查询用户所有角色信息
     *
     * @param userId
     * @return
     */
    @Override
    public List<Role> getRoleByUserId(String userId) {
        return roleMapper.getRoleByUserId(userId);
    }
}
