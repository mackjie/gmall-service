package com.gmall.user.server.utils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Project:gmall-user
 * File: com.gmall.user.server.utils
 *
 * @author : lh.Wu
 * @date : 2020/3/31
 * Copyright 2009-2020 www.gmall.cn, Ltd. All rights reserved.
 */
@Component
@Slf4j
public class CaffeineManager {

    Cache<String,Object> cache = Caffeine.newBuilder().build();
}
