package com.gmall.user.server.mapper;

import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.vo.ResourceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ResourceMapper {

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(Integer id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(Resource  record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    Resource  findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(Resource  record);

    /**
     * 唯一 条件查询 详情
     *
     * @param resourceQueryForm
     * @return
     */
    Resource  findByForm(ResourceQueryForm resourceQueryForm);

    /**
     * 根据角色id 查询该角色下全部（目录、菜单）权限
     *
     * @param userId
     * @param type
     * @return
     */
    List<ResourceVo> getResourceByUserId(@Param("userId") String userId, @Param("type") Integer type, @Param("parentId") Integer parentId);

    /**
     * 条件查询资源 数量
     *
     * @param resourceQueryForm
     * @return
     */
    int count(ResourceQueryForm resourceQueryForm);

    /**
     * 条件查询资源
     *
     * @param resourceQueryForm
     * @return
     */
    List<Resource> list(ResourceQueryForm resourceQueryForm);
}