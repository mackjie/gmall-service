package com.gmall.user.server.biz;

import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.RoleQueryForm;
import com.gmall.user.api.po.Role;
import com.gmall.user.server.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/6/9 13:58
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Component
public class RoleBiz {

    @Autowired
    private RoleService roleService;

    /**
     * 根据公司名称查询
     * 如果为
     * 公司 查询该公司所有权限
     *
     * @return
     */
    public Response<ResultList<Role>> getUserRole(RoleQueryForm roleQueryForm){
        Response response = Response.newInstance();
        try {
            ResultList<Role> result = ResultList.emptyList();
            // 根据公司id 查询所属公司所有权限列表
            int cnt = roleService.count(roleQueryForm);
            List<Role> list = roleService.list(roleQueryForm);
            result.setTotal(cnt);
            result.setResult(list);
            result.setPageSize(roleQueryForm.getPageSize());
            result.setPageNum(roleQueryForm.getPageNum());
            response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText(),result);
        } catch (Exception e) {
            response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        }
        return response;
    }
}
