package com.gmall.user.server.mapper;

import com.gmall.user.api.po.Position;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PositionMapper {
    int deleteById(Long id);

    int save(Position record);

    Position findById(Long id);

    int update(Position record);
}