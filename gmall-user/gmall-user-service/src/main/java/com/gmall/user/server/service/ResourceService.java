package com.gmall.user.server.service;

import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.vo.ResourceVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:25
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public interface ResourceService {
    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(Integer id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(Resource record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    Resource  findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(Resource  record);

    /**
     * 唯一 条件查询 详情
     *
     * @param resourceQueryForm
     * @return
     */
    Resource  findByForm(ResourceQueryForm resourceQueryForm);

    /**
     * 根据角色id 查询该角色下全部（目录、菜单）权限
     *
     * @param userId
     * @param type
     * @param parentId
     * @return
     */
    List<ResourceVo> getResourceByUserId(String userId, Integer type, Integer parentId);

    /**
     * 条件查询资源 数量
     *
     * @param resourceQueryForm
     * @return
     */
    int count(ResourceQueryForm resourceQueryForm);

    /**
     * 条件查询资源
     *
     * @param resourceQueryForm
     * @return
     */
    List<Resource> list(ResourceQueryForm resourceQueryForm);
}
