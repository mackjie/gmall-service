package com.gmall.user.server.mapper;

import com.gmall.user.api.form.UserInfoQueryForm;
import com.gmall.user.api.po.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserInfoMapper {
    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    int deleteById(String id);

    /**
     * 添加
     *
     * @param record
     * @return
     */
    int save(UserInfo record);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    UserInfo findById(String id);

    /**
     * 更新
     *
     * @param record
     * @return
     */
    int update(UserInfo record);

    /**
     * 唯一 条件查询 详情
     *
     * @param userInfoQueryForm
     * @return
     */
    UserInfo findByForm(UserInfoQueryForm userInfoQueryForm);
}