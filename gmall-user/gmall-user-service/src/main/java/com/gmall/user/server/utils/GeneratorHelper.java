/**
 * Project Name:
 * Class Name:com.sms.gmall.api.http.utlis.java
 * <p>
 * Version     Date         Author
 * -----------------------------------------
 * 1.0    2015年11月17日      HanKeQi
 * <p>
 * Copyright (c) 2019, gmall All Rights Reserved.
 */
package com.gmall.user.server.utils;

import com.gmall.common.util.Generator;

/**
 * @author HanKeQi
 * @Description
 * @date 2019/4/17 11:28 AM
 **/

public class GeneratorHelper {

    public static final Generator GENERATOR = new Generator(82);
}
