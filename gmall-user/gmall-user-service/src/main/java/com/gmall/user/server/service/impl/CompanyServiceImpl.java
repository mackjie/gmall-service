package com.gmall.user.server.service.impl;

import com.gmall.user.api.form.CompanyQueryForm;
import com.gmall.user.api.po.Company;
import com.gmall.user.server.mapper.CompanyMapper;
import com.gmall.user.server.service.CompanyService;
import org.springframework.stereotype.Service;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:26
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyMapper companyMapper;

    public CompanyServiceImpl(CompanyMapper companyMapper) {
        this.companyMapper = companyMapper;
    }

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    @Override
    public int deleteById(String id) {
        return companyMapper.deleteById(id);
    }

    /**
     * 添加
     *
     * @param record
     * @return
     */
    @Override
    public int save(Company record) {
        return companyMapper.save(record);
    }

    /**
     * 查询
     *
     * @param id
     * @return
     */
    @Override
    public Company findById(String id) {
        return companyMapper.findById(id);
    }

    /**
     * 更新
     *
     * @param record
     * @return
     */
    @Override
    public int update(Company record) {
        return companyMapper.update(record);
    }

    /**
     * 唯一 条件查询 详情
     *
     * @param companyQueryForm
     * @return
     */
    @Override
    public Company findByForm(CompanyQueryForm companyQueryForm) {
        return companyMapper.findByForm(companyQueryForm);
    }
}
