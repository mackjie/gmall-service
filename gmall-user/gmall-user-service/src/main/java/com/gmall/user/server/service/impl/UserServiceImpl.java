package com.gmall.user.server.service.impl;

import com.gmall.user.api.form.UserQueryForm;
import com.gmall.user.api.po.User;
import com.gmall.user.server.mapper.UserMapper;
import com.gmall.user.server.service.UserService;
import com.gmall.user.server.utils.GeneratorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Project:gmall-user
 * File: com.gmall.user.server.impl
 *
 * @author : lh.Wu
 * @date : 2019/12/2
 * Copyright 2006-2019 gmall Co., Ltd. All rights reserved.
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    @Override
    public int deleteById(String id) {
        return userMapper.deleteById(id);
    }

    /**
     * 添加
     *
     * @param record
     * @return
     */
    @Override
    public int save(User record) {
        return userMapper.save(record);
    }

    /**
     * 查询
     *
     * @param id
     * @return
     */
    @Override
    public User findById(String id) {
        return userMapper.findById(id);
    }

    /**
     * 更新
     *
     * @param record
     * @return
     */
    @Override
    public int update(User record) {
        return userMapper.update(record);
    }

    /**
     * 条件查询
     *
     * @param userQueryForm
     * @return
     */
    @Override
    public List<User> list(UserQueryForm userQueryForm) {
        return userMapper.list(userQueryForm);
    }

    /**
     * 条件数量统计
     *
     * @param userQueryForm
     * @return
     */
    @Override
    public Integer count(UserQueryForm userQueryForm) {
        return userMapper.count(userQueryForm);
    }

    /**
     * 唯一 条件查询 详情
     *
     * @param userQueryForm
     * @return
     */
    @Override
    public User findByForm(UserQueryForm userQueryForm) {
        return userMapper.findByForm(userQueryForm);
    }
}