package com.gmall.user.server.mapper;

import com.gmall.user.api.po.SysMsgLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SysMsgLogMapper {
    int deleteById(Long id);

    int save(SysMsgLog record);

    SysMsgLog findById(Long id);

    int update(SysMsgLog record);
}