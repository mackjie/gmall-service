package com.gmall.user.server.restapi;

import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.server.biz.ResourceBiz;
import com.gmall.user.server.service.ResourceService;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author HL.Wu
 * @date 2020/6/9 18:40
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@RestController
@Slf4j
@RequestMapping("/resource")
public class ResourceAPi {

    @Autowired
    private ResourceBiz resourceBiz;
    @Autowired
    private ResourceService resourceService;

    /**
     * 查询用户所拥有的角色信息
     *
     * @return
     */
    @PostMapping("/list")
    public Response<ResultList<Resource>> getRoleList(@RequestBody ResourceQueryForm resourceQueryForm){
        return resourceBiz.list(resourceQueryForm);
    }

    @PostMapping("/save")
    public int save(@RequestBody Resource resource){
        return resourceService.save(resource);
    }

    @PostMapping("/update")
    public int update(@RequestBody Resource resource){
        return resourceService.update(resource);
    }

    @GetMapping("/delete")
    public int delete(@RequestParam("id") @NotNull Integer id){
        return resourceService.deleteById(id);
    }
}
