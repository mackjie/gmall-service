package com.gmall.user.server.service.impl;

import com.gmall.user.api.form.UserInfoQueryForm;
import com.gmall.user.api.po.UserInfo;
import com.gmall.user.server.mapper.UserInfoMapper;
import com.gmall.user.server.service.UserInfoService;
import org.springframework.stereotype.Service;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:26
 * Copyright ©www.sioo.cn Copyright AII Right Reserve
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    private final UserInfoMapper userInfoMapper;

    public UserInfoServiceImpl(UserInfoMapper userInfoMapper) {
        this.userInfoMapper = userInfoMapper;
    }

    /**
     * 根据id 删除
     *
     * @param id
     * @return
     */
    @Override
    public int deleteById(String id) {
        return userInfoMapper.deleteById(id);
    }

    /**
     * 添加
     *
     * @param record
     * @return
     */
    @Override
    public int save(UserInfo record) {
        return userInfoMapper.save(record);
    }

    /**
     * 查询
     *
     * @param id
     * @return
     */
    @Override
    public UserInfo findById(String id) {
        return userInfoMapper.findById(id);
    }

    /**
     * 更新
     *
     * @param record
     * @return
     */
    @Override
    public int update(UserInfo record) {
        return userInfoMapper.update(record);
    }

    /**
     * 唯一 条件查询 详情
     *
     * @param userInfoQueryForm
     * @return
     */
    @Override
    public UserInfo findByForm(UserInfoQueryForm userInfoQueryForm) {
        return userInfoMapper.findByForm(userInfoQueryForm);
    }
}
