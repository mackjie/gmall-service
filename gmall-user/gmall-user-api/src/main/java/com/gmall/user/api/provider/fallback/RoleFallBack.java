package com.gmall.user.api.provider.fallback;

import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.RoleQueryForm;
import com.gmall.user.api.po.Role;
import com.gmall.user.api.provider.RoleClient;

/**
 * @author HL.Wu
 * @date 2020/6/9 18:55
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public class RoleFallBack implements RoleClient {
    /**
     * 查询用户所拥有的角色信息
     *
     * @param roleQueryForm
     * @return
     */
    @Override
    public Response<ResultList<Role>> getRoleList(RoleQueryForm roleQueryForm) {
        Response<ResultList<Role>> response = Response.newInstance();
        response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        return response;
    }

    @Override
    public int save(Role role) {
        return 0;
    }

    @Override
    public int update(Role role) {
        return 0;
    }

    @Override
    public int delete(Integer id) {
        return 0;
    }
}
