package com.gmall.user.api.vo;

import com.gmall.user.api.po.Resource;
import com.gmall.user.api.po.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 15:59
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class UserInfoVo extends UserInfo {

    /**
     * 所有资源菜单
     */
    private List<Resource> resources;

    /**
     * 用户所拥有的角色信息
     */
    private List<String> roles;
}
