package com.gmall.user.api.form;

import com.gmall.common.model.BaseQueryForm;
import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/5/28 10:43
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class UserQueryForm extends BaseQueryForm {
    /**
     * id
     */
    private String id;
    /**
     * 用户名称
     */
    private String username;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 类型 0：个人；1：公司
     */
    private Integer type;

    /**
     * 状态 0：正常；1：禁用
     */
    private Integer status;

    /**
     * 个人类型：用户详情表id
     */
    private String userInfoId;

    /**
     * 公司类型：公司详情表id
     */
    private String companyId;

    /**
     * 删除 0：正常；1：删除
     */
    private Integer isDeleted;

    /**
     * 密码
     */
    private String password;

    /**
     * 父id
     */
    private String parentId;

    /**
     * 微信unionId
     */
    private String wechatUnionid;

    /**
     * 邀请码
     */
    private String inviteCode;
}
