package com.gmall.user.api.vo;

import com.gmall.user.api.po.Resource;
import lombok.Data;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 15:56
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */

@Data
public class ResourceVo extends Resource {

   private List<ResourceVo> resources;
}
