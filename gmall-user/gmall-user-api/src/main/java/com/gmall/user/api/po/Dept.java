package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class Dept extends BasePo {

    /**
     * 部门名称 简称
     */
    private String deptName;

    /**
     * 部门全称
     */
    private String deptFullName;

    /**
     * 排序
     */
    private Integer orderNo;

    /**
     * 所属公司id
     */
    private Long companyId;

    /**
     * 备注
     */
    private String comment;

    /**
     * 所属父组织id
     */
    private Long parentId;
}