package com.gmall.user.api.provider;

import com.alibaba.fastjson.JSONObject;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.UserQueryForm;
import com.gmall.user.api.po.User;
import com.gmall.user.api.provider.fallback.UserFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author HL.Wu
 * @date 2020/5/28 10:46
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@FeignClient(name = "gmall-user-server",fallback = UserFallBack.class)
public interface UserClient {

    /**
     * 根据主键查询用户基本信息
     *
     * @param id
     * @return
     */
    @GetMapping("/user/findUserById")
    User findUserById(@RequestParam("id") String id);

    /**
     * 条件查询用户列表信息
     *
     * @param userQueryForm
     * @return
     */
    @PostMapping("/user/list")
    ResultList<User> list(@RequestBody UserQueryForm userQueryForm);

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @GetMapping("/user/findUserByName")
    User findUserByName(@RequestParam("userName") String userName);

    /**
     * 根据用户id 查询用户详细信息
     *
     * @param userId
     * @return
     */
    @GetMapping("/user/getAccountInfo")
    Response<JSONObject> getAccountInfo(@RequestParam("userId") String userId);

    /**
     * 用户注册保存
     *
     * @param jsonObject
     * @return
     */
    @PostMapping("/user/save")
    int save(@RequestBody JSONObject jsonObject);

    /**
     * 更新用户信息
     *
     * @param user
     * @return
     */
    @PostMapping("/user/update")
    int update(@RequestBody User user);

    @GetMapping("/user/delete")
    int delete(@RequestParam("id") String id);

}
