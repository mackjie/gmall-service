package com.gmall.user.api.form;

import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/5/28 13:52
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class CompanyQueryForm extends UserQueryForm {
    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司座机号码
     */
    private String mobile;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 公司法人
     */
    private String legalPerson;

    /**
     * 法人身份证号码
     */
    private String legalCard;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 公司邮箱
     */
    private String email;

    /**
     * 证券代码
     */
    private String stockCode;

    /**
     * 公司行业类别
     */
    private Integer category;
}
