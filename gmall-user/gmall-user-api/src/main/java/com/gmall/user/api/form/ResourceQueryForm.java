package com.gmall.user.api.form;

import com.gmall.common.model.QueryForm;
import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/5/28 14:05
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */

@Data
public class ResourceQueryForm extends QueryForm {
    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源code
     */
    private String code;

    /**
     * 所属公司 id
     */
    private String companyId;
}
