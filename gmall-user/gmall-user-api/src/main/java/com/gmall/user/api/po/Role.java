package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class Role extends BasePo {

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色code
     */
    private String code;

    /**
     * 备注
     */
    private String comment;

    /**
     * 删除 0：正常；1：删除
     */
    private String isDeleted;

    /**
     * 所属公司id
     */
    private String companyId;
}