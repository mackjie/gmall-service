package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class SysConfig extends BasePo {

    /**
     * 系统名称
     */
    private String configName;

    /**
     * 系统配置code
     */
    private String configCode;

    /**
     * 备注
     */
    private String comment;

    /**
     * 状态
     * 0：正常
     * 1：禁用
     */
    private Integer status;

}