package com.gmall.user.api.vo;

import com.gmall.user.api.po.Resource;
import lombok.Data;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/6/9 13:51
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class RoleVo {

    /**
     * 角色所拥有的资源
     */
    private List<Resource> resources;
}
