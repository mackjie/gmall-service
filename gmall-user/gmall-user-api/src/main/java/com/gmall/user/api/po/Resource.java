package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class Resource extends BasePo {

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源code
     */
    private String code;

    /**
     * 类型 0；目录 1：菜单 2：按钮
     */
    private Integer type;

    /**
     * 父id
     */
    private Integer parentId;

    /**
     * 平台类型 0：公司类型 1：个人类型
     */
    private Integer platformType;

    /**
     * 请求路径
     */
    private String requestPath;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序序号
     */
    private Integer sortNo;

    /**
     * 备注
     */
    private String comment;

    /**
     * 删除 0：正常；1：删除
     */
    private String isDeleted;

    /**
     * 所属公司id
     */
    private String companyId;
}