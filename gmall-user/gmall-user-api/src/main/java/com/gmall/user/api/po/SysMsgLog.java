package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class SysMsgLog extends BasePo {

    /**
     * 配置名称
     */
    private String configName;

    /**
     * 配置code
     */
    private String configCode;

    /**
     * 备注
     */
    private String comment;

    /**
     * 状态
     * 0：正常
     * 1：禁用
     */
    private Integer status;

    /**
     * 0：正藏
     * 1：删除
     */
    private Integer isDeleted;

}