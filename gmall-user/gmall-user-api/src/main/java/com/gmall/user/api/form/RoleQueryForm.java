package com.gmall.user.api.form;

import com.gmall.common.model.BaseQueryForm;
import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/5/28 13:41
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class RoleQueryForm extends BaseQueryForm {
    /**
     * id
     */
    private Integer id;
    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色code
     */
    private String code;

    /**
     * 所属公司id
     */
    private String companyId;

}
