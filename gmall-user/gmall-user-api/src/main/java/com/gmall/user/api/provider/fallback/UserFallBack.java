package com.gmall.user.api.provider.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.UserQueryForm;
import com.gmall.user.api.po.User;
import com.gmall.user.api.provider.UserClient;

/**
 * @author HL.Wu
 * @date 2020/5/28 13:06
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public class UserFallBack implements UserClient {
    /**
     * 根据主键查询用户基本信息
     *
     * @param id
     * @return
     */
    @Override
    public User findUserById(String id) {
        return new User();
    }

    @Override
    public ResultList<User> list(UserQueryForm userQueryForm) {
        return ResultList.emptyList();
    }

    @Override
    public User findUserByName(String userName) {
        return null;
    }

    @Override
    public Response<JSONObject> getAccountInfo(String userId) {
        Response<JSONObject> response = Response.newInstance();
        return response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
    }


    @Override
    public int save(JSONObject jsonObject) {
        return 0;
    }

    @Override
    public int update(User user) {
        return 0;
    }

    @Override
    public int delete(String id) {
        return 0;
    }
}
