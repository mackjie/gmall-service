package com.gmall.user.api.utils;

import java.util.Calendar;

/**
 * Project:gmall-user
 * File: com.gmall.user.api.utils
 *
 * @author : lh.Wu
 * @date : 2020/3/31
 * Copyright 2009-2020 www.gmall.cn, Ltd. All rights reserved.
 */
public class DateUtilCommon {
    
    /**
      * 获取当月的 天数
      */
    public static int getCurrentMonthDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }
}
