package com.gmall.user.api.po;

import lombok.Data;

@Data
public class Company extends User {

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司座机号码
     */
    private String mobile;

    /**
     * 公司地址
     */
    private String address;

    /**
     * 公司logo
     */
    private String logo;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 公司行业类别
     */
    private Integer category;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 公司法人
     */
    private String legalPerson;

    /**
     * 法人身份证号码
     */
    private String legalCard;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 公司邮箱
     */
    private String email;

    /**
     * 证券代码
     */
    private String stockCode;

    /**
     * 公司简介
     */
    private String description;

}