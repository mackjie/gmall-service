package com.gmall.user.api.provider;

import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.provider.fallback.ResourceFallBack;
import com.sun.istack.NotNull;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author HL.Wu
 * @date 2020/6/9 18:52
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@FeignClient(value = "gmall-user-server",fallback = ResourceFallBack.class)
public interface ResourceClient {
    /**
     * 查询用户所拥有的角色信息
     *
     * @return
     */
    @PostMapping("/role/list")
    Response<ResultList<Resource>> getRoleList(@RequestBody ResourceQueryForm resourceQueryForm);

    @PostMapping("/role/save")
    int save(@RequestBody Resource resource);

    @PostMapping("/role/update")
    int update(@RequestBody Resource resource);

    @GetMapping("/role/delete")
    int delete(@RequestParam("id") @NotNull Integer id);
}
