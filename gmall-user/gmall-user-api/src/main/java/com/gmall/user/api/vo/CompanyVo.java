package com.gmall.user.api.vo;

import com.gmall.user.api.po.Company;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.po.Role;
import lombok.Data;

import java.util.List;

/**
 * Project:gmall-user
 * File: com.gmall.user.api.vo
 *
 * @author : lh.Wu
 * @date : 2020/4/20
 * Copyright 2009-2020 www.gmall.cn, Ltd. All rights reserved.
 */
@Data
public class CompanyVo extends Company {

    /**
     * 所有资源菜单
     */
    private List<Resource> resources;

    /**
     * 用户所拥有的角色信息
     */
    private List<String> roles;
}
