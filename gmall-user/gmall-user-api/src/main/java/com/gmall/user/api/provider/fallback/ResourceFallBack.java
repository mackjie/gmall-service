package com.gmall.user.api.provider.fallback;

import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.provider.ResourceClient;

/**
 * @author HL.Wu
 * @date 2020/6/9 18:57
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public class ResourceFallBack implements ResourceClient {
    /**
     * 查询用户所拥有的角色信息
     *
     * @param resourceQueryForm
     * @return
     */
    @Override
    public Response<ResultList<Resource>> getRoleList(ResourceQueryForm resourceQueryForm) {
        Response<ResultList<Resource>> response = Response.newInstance();
        response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        return response;
    }

    @Override
    public int save(Resource resource) {
        return 0;
    }

    @Override
    public int update(Resource resource) {
        return 0;
    }

    @Override
    public int delete(Integer id) {
        return 0;
    }
}
