package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class SysMsgTemplate extends BasePo {

    /**
     * 状态
     * 0：正常
     * 1：禁用
     */
    private Integer status;

    /**
     * 类型
     * 消息类型，msg_notice:通知类型；msg_event:事件类型；msg_alarm:告警类型；msg_remind:提醒类型
     */
    private String type;

    /**
     * 发送渠道，sys_msg:站内信；email:邮件；sms:短信；wechat:微信；enterprise_wechat:企业微信
     */
    private String channel;

    /**
     * 主题
     */
    private String title;

    /**
     * 消息来源，platform:平台；devops:运维；channel:通道
     */
    private String msgFrom;

    /**
     * 接收者类型 0：个人；1：公司；2：所有
     */
    private Integer receiverType;

    /**
     * 备注
     */
    private String comment;

}