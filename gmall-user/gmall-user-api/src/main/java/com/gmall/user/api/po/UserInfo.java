package com.gmall.user.api.po;

import lombok.Data;

@Data
public class UserInfo extends User{

    /**
     * 头像
     */
    private String avatar;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 联系人姓名
     */
    private String contacts;

    /**
     * 个人邮箱
     */
    private String email;

    /**
     * 地址
     */
    private String address;

    /**
     * 关注类别
     */
    private Integer category;
}