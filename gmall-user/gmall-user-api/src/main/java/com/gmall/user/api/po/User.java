package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class User extends BasePo {

    /**
     * 用户名称
     */
    private String username;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 类型 0：个人；1：公司
     */
    private Integer type;

    /**
     * 状态 0：正常；1：禁用
     */
    private Integer status;

    /**
     * 父id
     */
    private String parentId;

    /**
     * 个人类型：用户详情表id
     */
    private String userInfoId;

    /**
     * 公司类型：公司详情表id
     */
    private String companyId;

    /**
     * 绑定微信关联id
     */
    private String wechatUnionid;

    /**
     * 邀请码
     */
    private String inviteCode;

    /**
     * 邀请数量
     */
    private Integer inviteCnt;

    /**
     * 支付密码
     */
    private String payPassword;

    /**
     * 过期时间
     */
    private Date expireDate;

    /**
     * 删除 0：正常；1：删除
     */
    private Integer isDeleted;

    /**
     * 所有角色
     */
    private List<String> roles;

    /**
     * 是否记住密码
     */
    private Integer rememberMe;

    /**
     * 0、未知，1、男，2、女
     */
    private Integer sex;

    /**
     * 页面导航面包屑是否展示
     *
     * 0：展示；1：隐藏
     */
    private Integer tagsView;

    /**
     * 头部logo是否显示
     *
     * 0：展示；1：隐藏
     */
    private Integer headerLogoView;

    /**
     * 客户重要性等级
     */
    private Integer importance;
}