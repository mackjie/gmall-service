package com.gmall.user.api.form;

import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/5/28 13:51
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class UserInfoQueryForm extends UserQueryForm{
    /**
     * 手机号码
     */
    private String phone;

    /**
     * 联系人姓名
     */
    private String contacts;

    /**
     * 个人邮箱
     */
    private String email;
}
