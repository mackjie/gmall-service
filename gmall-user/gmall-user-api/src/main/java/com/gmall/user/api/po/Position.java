package com.gmall.user.api.po;

import com.gmall.common.model.BasePo;
import lombok.Data;

@Data
public class Position extends BasePo {

    /**
     * 职位名称
     */
    private String positionName;

    /**
     * 职位code
     */
    private String positionCode;

    /**
     * 所属公司id
     */
    private String companyId;

    /**
     * 备注
     */
    private String comment;

    /**
     * 状态
     * 0；正常
     * 1：禁用
     */
    private Integer status;
}