package com.gmall.common.util;

import com.google.common.collect.Maps;
import org.apache.commons.collections.MapUtils;

import java.util.Map;
import java.util.Optional;

/**
 * 用户上下文
 */
public class UserContextHolder {

    private ThreadLocal<Map<String, Object>> threadLocal;

    private UserContextHolder() {
        this.threadLocal = new ThreadLocal<>();
    }

    /**
     * 创建实例
     *
     * @return
     */
    public static UserContextHolder getInstance() {
        return SingletonHolder.sInstance;
    }

    /**
     * 获取上下文中的信息
     *
     * @return
     */
    public Map<String, Object> getContext() {
        return threadLocal.get();
    }

    /**
     * 用户上下文中放入信息
     *
     * @param map
     */
    public void setContext(Map<String, Object> map) {
        threadLocal.set(map);
    }

    /**
     * 获取上下文中的用户名
     *
     * @return
     */
    public String getUsername() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("userName");
    }

    /**
     * 获取上下文中的租户id
     *
     * @return
     */
    public String getTenantId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("tenantId");
    }

    /**
     * 获取userId
     *
     * @return
     */
    public String getUserId() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("userId");
    }

    /**
     * 获取用户类型
     *
     * @return
     */
    public int getType() {
        Map<String, Object> map = threadLocal.get();
        if (MapUtils.isEmpty(map)) {
            return 0;
        }
        return (Integer) map.get("type");
    }

    /**
     * 清空上下文
     */
    public void clear() {
        threadLocal.remove();
    }

    /**
     * 静态内部类单例模式
     * 单例初使化
     */
    private static class SingletonHolder {
        private static final UserContextHolder sInstance = new UserContextHolder();
    }

}
