package com.gmall.common.util;

import java.util.UUID;

public class UUIDUtils {

    /**
     * 生成UUID
     *
     * @param
     * @return
     */
    public static String UUID() {
        UUID uuid = UUID.randomUUID();
        return String.valueOf(uuid).replaceAll("-", "");
    }
}
