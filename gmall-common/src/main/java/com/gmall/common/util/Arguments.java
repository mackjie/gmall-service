package com.gmall.common.util;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import java.util.Collection;
import java.util.List;

/**
 * 基本的参数校验方法 包括集合
 *
 * @author Vinent
 */
public final class Arguments {

    public static boolean isNull(Object o) {
        return o == null;
    }

    public static boolean notNull(Object o) {
        return o != null;
    }

    public static boolean isEmpty(String s) {
        return Strings.isNullOrEmpty(s);
    }

    public static boolean notEmpty(String s) {
        return !isEmpty(s);
    }

    @SuppressWarnings("rawtypes")
    public static <T extends List> boolean isNullOrEmpty(T t) {
        return (isNull(t)) || (isEmpty(t));
    }

    public static boolean positive(Number n) {
        return n.doubleValue() > 0.0D;
    }

    public static boolean negative(Number n) {
        return n.doubleValue() < 0.0D;
    }

    public static boolean not(Boolean t) {
        Preconditions.checkArgument(notNull(t));
        return !t.booleanValue();
    }

    public static <T> boolean equalWith(T source, T target) {
        return Objects.equal(source, target);
    }

    @SuppressWarnings("rawtypes")
    public static <T extends Collection> boolean isEmpty(T t) {
        return t.isEmpty();
    }

    @SuppressWarnings("rawtypes")
    public static <T extends Collection> boolean notEmpty(T l) {
        return (notNull(l)) && (!l.isEmpty());
    }


}