package com.gmall.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author hl.wu
 * 内容工具类
 */
public class ContentUtils {

    /**
     * 匹配特殊字符
     */
    public static Pattern pattern = Pattern.compile("[^a-z*0-9._ ]", Pattern.CASE_INSENSITIVE);

    /**
     * 计算内容短信条数
     *
     * @param content 内容
     * @return int
     */
    public static int contentNum(String content) {
        // 重新计算条数
        int contentLength = content.length();
        int cCount = contentLength > 70 ? 67 : 70;
        if (contentLength % cCount != 0) {
            cCount = (contentLength / cCount) + 1;
        } else {
            cCount = contentLength / cCount;
        }
        return cCount;
    }

    /**
     * 如果存在返回true
     *
     * @param str
     * @return
     */
    public static boolean existSpecialChar(String str) {
        Matcher m = pattern.matcher(str);
        return m.find();
    }

}
