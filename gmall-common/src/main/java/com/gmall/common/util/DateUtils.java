package com.gmall.common.util;


import org.joda.time.DateTime;

import java.util.Date;

/**
 * Date基础工具类
 *
 * @author hl.wu
 */
public class DateUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";


    public static Date startOfDay(Date date) {
        if (date == null) {
            return null;
        }
        return new DateTime(date).withTimeAtStartOfDay().toDate();
    }

    public static Date endOfDay(Date date) {
        if (date == null) {
            return null;
        }
        return new DateTime(date).withTimeAtStartOfDay().plusDays(1).toDate();
    }
}