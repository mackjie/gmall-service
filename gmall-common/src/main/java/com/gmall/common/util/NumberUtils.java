package com.gmall.common.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 数字工具类
 *
 * @author Vincent
 */
public class NumberUtils {

    private static final DecimalFormat df = new DecimalFormat("0.0000");
    //厘/元转换
    private static final BigDecimal unit_price = new BigDecimal(1000);

    private static final String DATE_FORMAT = "yyyyMMddHHmm";

    /**
     * 将厘单位的价格转换为元 精确到里
     *
     * @param price
     * @return
     */
    public static String formatPricetoRMB(Number price) {
        if (price == null) {
            return "";
        }
        BigDecimal bPrice = new BigDecimal(Double.toString(price.doubleValue()));
        return df.format(bPrice.divide(unit_price).doubleValue());
    }

    /**
     * 将元为单位的转换为里
     *
     * @param price
     * @return 0 非法数字
     */
    public static Number formatPricetoLi(String price) {

        BigDecimal bPrice = new BigDecimal(price);

        if (isNumeric(price)) {
            return 0;
        }
        return bPrice.multiply(unit_price).doubleValue();
    }

    public static boolean gt0(List<Long> nums) {
        if ((nums == null) || (nums.size() < 0)) {
            return false;
        }
        for (Long num : nums) {
            if (!gt0(num)) {
                return false;
            }
        }
        return true;
    }

    public static boolean gt0(Integer num) {
        return (num != null) && (num.intValue() > 0);
    }

    public static boolean gt0(Long num) {
        return (num != null) && (num.longValue() > 0L);
    }

    public static boolean isNumeric(String str) {
        boolean bool = false;
        try {
            Double.parseDouble(str);
        } catch (NumberFormatException e) {
            bool = true;
        }
        return bool;
    }

    public static Number add(double d1, double d2) {
        BigDecimal b1 = new BigDecimal(Double.toString(d1));
        BigDecimal b2 = new BigDecimal(Double.toString(d2));
        return b1.add(b2).doubleValue();
    }

    /**
     * @param value 转换值
     * @param point 精确到几位
     * @return
     */
    public static double formatDouble(Double value, Integer point) {
        return formatDouble(String.valueOf(value), point);
    }

    /**
     * @param value 转换值
     * @param point 精确到几位
     * @return
     */
    public static double formatDouble(String value, Integer point) {
        if (value == null) {
            return 0.0;
        }
        BigDecimal decimal = new BigDecimal(value);
        decimal = decimal.setScale(point, BigDecimal.ROUND_HALF_DOWN);
        return decimal.doubleValue();
    }

    /**
     * @param value 转换值
     * @return
     */
    public static Double formatDouble(Double value) {
        if (value == null) {
            return 0.0;
        }
        return formatDouble(String.valueOf(value));
    }

    /**
     * @param value String 类型 转换值
     * @return
     */
    public static Double formatDouble(String value) {
        if (value == null) {
            return 0.0;
        }
        BigDecimal decimal = new BigDecimal(value);
        decimal = decimal.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        return decimal.doubleValue();
    }

    /**
     * 生成订单号
     *
     * @param userId
     * @return
     */
    @Deprecated
    public synchronized String generateOrderNoByUserId(String userId) {
        if (StringUtils.isNotBlank(userId)) {
            return null;
        }
        StringBuffer orderNoBuffer = new StringBuffer();
        orderNoBuffer.append(DateFormatUtils.format(new Date(), DATE_FORMAT))
                .append(StringUtils.substring(userId, userId.length() - 3))
                .append(new Random().nextInt(100));
        return orderNoBuffer.toString();
    }
}
