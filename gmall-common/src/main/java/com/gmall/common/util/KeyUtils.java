package com.gmall.common.util;

/**
 * redis key統一命名
 *
 * @author Vincent
 */
public abstract class KeyUtils {

    //用户信息
    public static String userInfoByUserId(String userId) {
        return "user-info-userId:" + userId;
    }

    //通道组关系
    public static String groupRelationByTenantId(String groupId, String tenantId) {
        return "channel-group-relation-info:" + groupId + ":" + tenantId;
    }

    //用户余额
    public static String userBalanceByUserId(String userId) {
        return "user-balance:" + userId;
    }

    //商户余额
    public static String tenantBalanceByTenantId(String tenantId) {
        return "tenant-balance:" + tenantId;
    }

    //用户冻结余额
    public static String userFrozenBalanceByUserId(String userId, Integer productCode) {
        return "user-frozen-balance:" + userId + ":" + productCode;
    }

    //商户冻结余额
    public static String tenantFrozenBalanceByTenantId(String tenantId, Integer productCode) {
        return "tenant-frozen-balance:" + tenantId + ":" + productCode;
    }

    //用户SEO产品余额
    public static String userSeoBalanceByUserId(String userId, Integer productCode) {
        return "user-seo-balance:" + userId + ":" + productCode;
    }

    //通道信息
    public static String channelInfoByChannelId(String channelId) {
        return "channel-info:" + channelId;
    }

    //用户白名单
    public static String userWhiteMobileByUserId(String userId, String mobile) {
        return "user-white-mobile:" + userId + ":" + mobile;
    }

    //用户黑名单
    public static String userBlackMobileByUserId(String userId, String mobile) {
        return "user-black-mobile:" + userId + ":" + mobile;
    }

    //用户审核词
    public static String userAuditWordsByUserId(String userId) {
        return "user-audit-words:" + userId;
    }

    //用户屏蔽词
    public static String userBlackWordsByUserId(String userId) {
        return "user-black-words:" + userId;
    }

    //系统屏蔽词
    public static String sysBlackWords(String tenantId) {
        return "sys-black-words:" + tenantId;
    }

    //系统审核词
    public static String sysAuditWords(String tenantId) {
        return "sys-audit-words:" + tenantId;
    }

    //系统黑名单
    public static String sysBlackMobile(String tenantId, String mobile) {
        return "sys-black-mobile:" + tenantId + ":" + mobile;
    }

    //系统白名单
    public static String sysWhiteMobile(String tenantId, String mobile) {
        return "sys-white-mobile:" + tenantId + ":" + mobile;
    }

    //用户屏蔽地区

    /**
     * @param userId       用户id
     * @param provinceCode 省编号
     * @return
     */
    public static String userBlackAreaByUserId(String userId, Integer provinceCode) {
        return "user-black-area:" + provinceCode + ":" + userId;
    }


    /**
     * 根据userId查询user的List<UserTemplateVerify>
     *
     * @param userId
     * @return 获取用户的所有可用审核模板
     */
    public static String userTemplateVerifyByUserId(String userId) {
        return "user-template:" + userId;
    }

    /**
     * 根据channelId,接口类型查询api的JSONObject
     *
     * @param channelId
     * @return 获取通道的可用的api
     */
    public static String channleApiBychannelId(String channelId, String type) {
        return "channel-api:" + channelId + ":" + type;
    }

    /**
     * 根据userId查询用户签名信息List<Signature>
     *
     * @param userId
     * @return 获取用户的所有可用签名
     */
    public static String userSignatureInfoByUserId(String userId) {
        return "user-signature-info:" + userId;
    }

    /**
     * 根据singatureId 查询签名通道关联信息 List<SignatureChannelInfo>
     *
     * @param signatureId 签名id
     * @return
     */
    public static String signatureChannelInfoBySignatureId(String signatureId) {
        return "signature-channel-relation:" + signatureId;
    }

    /**
     * 验证码短信订单key
     *
     * @param userId
     * @return
     */
    public static String getIdentifyCodeKey(String userId) {
        return userId + ":identify-code";
    }

    /**
     * 通知短信订单key
     *
     * @param userId
     * @return
     */
    public static String getNotifyCodeKey(String userId) {
        return userId + ":notify-code";
    }

    /**
     * 营销短信key
     *
     * @param userId
     * @return
     */
    public static String getGeneralizeCodeKey(String userId) {
        return userId + ":generalize-code";
    }

    /**
     * 订单信息key
     *
     * @param orderId userId
     * @return
     */
    public static String getOrderInfoKey(String orderId, String userId) {
        return orderId + ":bill:" + userId;
    }

    /**
     * 根据短信类型获取用户价格
     *
     * @param messageType 短信类型: 验证码: identify   通知:notify  营销: generalize
     * @param userId
     * @see MessageType
     */
    public static String getUserPrice(String messageType, String userId) {
        return messageType + "-price:" + userId;
    }

    /**
     * 根据 短信类型 和  运营商类型 获取通道组信息 key
     *
     * @param messageType 短信类型: 验证码: identify   通知:notify  营销: generalize
     * @param mobileType  运营商类型: 移动:cmcc  , 联通 : cucc ,电信: ctcc
     * @see MessageType
     * @see MobileType
     */
    public static String getChannelGroupKey(String messageType, String mobileType, String userId) {
        return messageType + "-group:" + mobileType + ":" + userId;
    }

    /**
     * 获取用户月度消费
     *
     * @param userId
     * @param monthStr yyyy-MM
     * @return
     */
    public static String getMonthConsumeByUserId(String userId, String monthStr) {
        return "consume:" + userId + ":" + monthStr;
    }

    /**
     * 获取用户日消费
     *
     * @param userId
     * @param dayStr yyyy-MM-dd
     * @return
     */
    public static String getDayConsumeByUserId(String userId, String dayStr) {
        return "consume:" + userId + ":" + dayStr;
    }


    /**
     * 获取用户使用商城通道日消费
     *
     * @param userId
     * @param dayStr yyyy-MM-dd
     * @return
     */
    public static String getChannelItemConsume(String channelId, String userId, String dayStr) {
        return "ChannelItemConsume:" + channelId + ":" + userId + ":" + dayStr;
    }


    /**
     * 获取用户省网通道组
     *
     * @param userId
     * @return
     */
    public static String getProvinceChannGroupIdByUserId(String userId) {
        return "provinceChannelGroupId:" + userId;
    }


    /**
     * 用户发送情况
     *
     * @param userId 用户id
     * @param price  用户价格
     * @param dayStr 日期 yyyy-MM-dd
     * @return
     * @see SendInfoConstant
     */
    public static String getUserSendInfo(String userId, String dayStr, double price) {
        return "userSend:" + userId + ":" + dayStr + ":" + price;
    }

    /**
     * 通道发送统计key
     *
     * @param channelId 通道id
     * @param price     用户价格
     * @param dayStr    日期 yyyy-MM-dd
     * @return
     */
    public static String getChannelSendInfo(String channelId, String dayStr, double price) {
        return "channelSend:" + channelId + ":" + dayStr + ":" + price;
    }

    /**
     * 通道商城发送统计key
     *
     * @param channelId    最终发送出去通道id
     * @param userId       用户id
     * @param dayStr       日期 yyyy-MM-dd
     * @param userPrice    用户发送价格
     * @param channelPrice 通道价格
     * @return
     */
    public static String getChannelMallSendInfo(String channelId, String userId, String dayStr, double userPrice, double channelPrice) {
        return "channelMallSend:" + channelId + ":" + userId + ":" + dayStr + ":" + userPrice + ":" + channelPrice;
    }

    /**
     * 根据userId +  md5Value 获取用户的待审核批次id
     * <p>
     * 获取 用户 待审核批次
     *
     * @param userId
     * @param md5Value
     * @return
     */
    public static String getVerifyId(String userId, String md5Value) {
        return "verify:" + userId + ":" + md5Value;
    }

    /**
     * 对租户端tenant进行搜索补发进行限制:
     * (1) 每次都校验有没有key
     * (2) 搜索补发完毕删除key
     *
     * @param tenantId
     * @return
     */
    public static String getResendByQueryKey(String tenantId) {
        return "resendByQuery:" + tenantId;
    }

    /**
     * 已登录用户生成的token
     *
     * @param userId
     * @return
     */
    public static String getUserLoginToken(String userId) {
        return "user-login-token:" + userId;
    }

    /**
     * 获取商城通道信息
     * 存储的是 channelMall 对象
     *
     * @param channelMallId 商城通道id
     * @return
     */
    public static String getChannelMall(String channelMallId) {
        return "channelMall:" + channelMallId;
    }

    /**
     * 获取商城通道配置
     * 存储的是 channelMallConfig 对象
     *
     * @param channelMallId 商城通道id
     * @return
     */
    public static String getChannelMallConfig(String channelMallId) {
        return "channelMallConfig:" + channelMallId;
    }

    /**
     * 通道接入信息
     * 存储的是 channelMallConnect 对象
     *
     * @param tenantId  租户id
     * @param channelId 通道id
     * @return
     */
    public static String getChannelMallConnect(String tenantId, String channelId) {
        return "channelMallConnect:" + tenantId + ":" + channelId;
    }

    /**
     * 关键词路由策略
     * 存储的是路由策略对象
     *
     * @param strategyId 策略id
     * @return
     */
    public static String getWordRouteStrategy(String strategyId) {
        return "wordRouteStrategy:" + strategyId;
    }

    /**
     * 关键词路由详情
     * 存储的是 关键词路由详情对象集合
     *
     * @param strategyId 策略id
     * @return
     */
    public static String getWordRouteDetail(String strategyId) {
        return "wordRouteDetail:" + strategyId;
    }

    /**
     * 定时任务key
     *
     * @param time
     * @return
     */
    public static String getJobTimeTask(String time, String taskName) {
        return "jobTiming:" + taskName + ":" + time;
    }

    /**
     * 校验审核数量限制
     *
     * @param md5      md5(userId + content + channelId)
     * @param dataTime yyyy-MM-dd
     * @return
     */
    public static String getVerifyAmount(String md5, String dataTime) {
        return "VerifyAmount:" + md5 + dataTime;
    }

    /**
     * 获取用户提醒标志
     *
     * @param type
     * @param userId
     * @param dateTime
     * @return
     */
    public static String getUserMaxSendOneMonth(String type,String userId,String dateTime){
        return "userMaxSendOneMonth:" + type + ":" + dateTime + ":" + userId;
    }

    /**
     * 用户当天最大发送条数
     *
     * @param type
     * @param userId
     * @param dateTime
     * @return
     */
    public static String getUserMaxSendOneDay(String type,String userId,String dateTime){
        return "userMaxSendOneDay:" + type+ ":" + userId + ":" + dateTime ;
    }

    /**
     * 携号转网查询 key
     *
     * @param mobile
     * @return
     */
    public static String getMobileChangeSp(String mobile) {
        return "mobileChangeSp:" + mobile;
    }
}