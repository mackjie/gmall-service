package com.gmall.common.enums;

import lombok.Getter;

/**
 * @author hl.wu
 * 用户回执 推送方式
 */
@Getter
public enum UserPushType {
    POST, GET
}
