package com.gmall.common.enums;

/**
 * @author HL.Wu
 * @date 2020/7/10 13:49
 * gmall-service
 * for:
 */
public enum DbEnum {
    //主数据库 --- 》读写
    MASTER,
    //副数据库---》只读
    SLAVE
}
