package com.gmall.common.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hl.wu
 */
@Data
public class BaseVo implements Serializable {

    private static final long serialVersionUID = 4228502664489478166L;

    private String id;

    private String creator;

    private String updater;

    private String createDate;

    private String updateDate;
}
