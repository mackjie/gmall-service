package com.gmall.common.model;

import com.gmall.common.enums.HttpBizCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * Project:gmall-common
 * File: com.gmall.cloud.common.core.model
 *
 * @author : hl.wu
 * @date : 2018-12-21
 * Copyright 2006-2018 gmall Co., Ltd. All rights reserved.
 */

/**
 * 接口返回结果类
 *
 * @author dean
 */
@ToString
public class Response<T> implements Serializable {

    private static final long serialVersionUID = -8912267628340132131L;

    private int code = HttpBizCode.SUCCESS.getCode();
    private String message = "SUCCESS";
    private T data;

    public Response(HttpBizCode code, String message, T data) {
        this.fill(code, message, data);
    }

    public Response(HttpBizCode code, String message) {
        this.fill(code, message, null);
    }

    public Response() {

    }

    public static <T> Response<T> newInstance() {
        return new Response<>();
    }

    /**
     * 成功调用
     *
     * @param code
     * @param message
     * @param data
     * @return
     */
    public Response<T> fill(HttpBizCode code, String message, T data) {
        this.code = code == null ? HttpBizCode.SUCCESS.getCode() : code.getCode();
        this.message = message;
        this.data = data;
        return this;
    }

    /**
     * 成功调用
     *
     * @param code
     * @param message
     * @return
     */
    public Response<T> fill(HttpBizCode code, String message) {
        this.code = code == null ? HttpBizCode.SUCCESS.getCode() : code.getCode();
        this.message = message;
        return this;
    }

    /**
     * 失败调用  code必须不等于200
     *
     * @param code
     * @param message
     * @return
     */
    public Response<T> fail(HttpBizCode code, String message) {
        this.code = code == null ? HttpBizCode.SUCCESS.getCode() : code.getCode();
        this.message = message;
        return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    @SuppressWarnings("rawtypes")
    public Response setData(T data) {
        this.data = data;
        return this;
    }
}