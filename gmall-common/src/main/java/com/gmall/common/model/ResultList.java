package com.gmall.common.model;

/**
 * Project:gmall-common
 * File: com.gmall.cloud.common.core.model
 *
 * @author : hl.wu
 * @date : 2018-12-21
 * Copyright 2006-2018 gmall Co., Ltd. All rights reserved.
 */

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 查询结果List,适用于分页，或者需要知道总记录数
 *
 * @param <E>
 * @author dean
 */
@Data
public class ResultList<E> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 每页大小
     */
    private int pageSize;

    /**
     * 页数
     */
    private int pageNum;

    /**
     * 数据总数
     */
    private int total = 0;

    /**
     * 结果
     */
    private List<E> result = new ArrayList<E>();

    public ResultList() {
    }

    public ResultList(int pageSize, int pageNum, int total, List<E> result) {
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.total = total;
        this.result = result;
    }

    public static <E> ResultList<E> emptyList() {
        return new ResultList<E>();
    }
}
