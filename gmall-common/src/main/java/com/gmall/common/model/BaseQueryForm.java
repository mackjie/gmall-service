package com.gmall.common.model;

import lombok.Data;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/28 10:23
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class BaseQueryForm extends QueryForm{
    /**
     * 授权角色绑定用户id
     */
    private List<String> userIdList;
}
