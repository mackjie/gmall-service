package com.gmall.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hl.wu
 */
@Data
public class BasePo implements Serializable {

    private static final long serialVersionUID = 7451057995073664208L;

    private Object id;

    private String creator;

    private String updater;

    private Date createDate = new Date();

    private Date updateDate = new Date();
}
