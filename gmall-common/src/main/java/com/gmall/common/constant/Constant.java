package com.gmall.common.constant;

/**
*
 * @author HL.Wu
 * @date 2020/5/22 14:21
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
public class Constant {

    public static final String USER_ID = "userId";

    public static final String COMPANY_ID = "company_id";

    public static final String JWT_SECRET = "gmall";

    /**
     * 用户
     */
    public static final String ISS = "gmall";

    public static final String REMEMBER_ME = "REMEMBER_ME";
}

