package com.gmall.common.constant;


/**
 * 系统配置
 *
 * @author hl.wu
 * @date 2019-11-09
 */
public class SystemConstant {

    /**
     * 用户金额转换基数:余额,单价,账单
     * 元 <--> 厘
     */
    public static final Integer PRICE_CHANGE_TIME = 1000;

}
