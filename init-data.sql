
--  初始化菜单
insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('机器人','ROBOT_WORK',0,0,null,'icon-shouye',0,'工作台',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('工作台','WORK_PLATE',1,7,'/work/list',null,1,'工作台',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('任务中心','TASK_CENTER',1,7,'/task/center',null,2,'任务中心',now(),now());


insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('我的话术','MY_WORDS',1,7,'/work/list',null,3,'我的话术',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('话术市场','WORD_MARKET',1,7,'/work/market',null,4,'话术市场',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('统计分析','STATISTICAL_ANALYSIS',1,7,null,null,5,'统计分析',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('通话统计','CALL_STATISTICS',1,12,'/work/list',null,1,'通话统计',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('意向统计','INTENTION_STATISTICS',1,12,'/statistics/intention',null,2,'意向统计',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('费用统计','TOTAL_EXPENSES',1,12,'/statistics/free',null,3,'费用统计',now(),now());

insert into sys_resource(
	`name`,`code`,`type`,`parent_id`,`request_path`,`icon`,`sort_no`,`comment`,`create_date`,`update_date`
) values
('免打扰名单','DISTURB_FREE',1,7,'/disturb/free',null,9,'免打扰名单',now(),now());


--  初始化角色

insert into sys_role(`id`,`name`,`code`,`comment`,`create_date`,`update_date`)
VALUES (1,'系统管理员','Admin','系统管理员',now(),now());

insert into sys_role(`id`,`name`,`code`,`comment`,`create_date`,`update_date`)
VALUES (2,'销售部','Sales','销售部',now(),now());

insert into sys_role(`id`,`name`,`code`,`comment`,`create_date`,`update_date`)
VALUES (3,'财务部','FINANCE_DEPARTMENT','财务部',now(),now());

insert into sys_role(`id`,`name`,`code`,`comment`,`create_date`,`update_date`)
VALUES (4,'技术部','TECHNOLOGY_DEPARTMENT','技术部',now(),now());

--  初始化权限资源
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,7,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,8,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,9,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,10,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,11,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,12,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,13,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,14,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,15,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (1,16,now(),now());

insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (2,8,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (3,10,now(),now());
insert into sys_role_resource(`role_id`,`resource_id`,`create_date`,`update_date`) VALUES (4,8,now(),now());

--  初始化用户角色关联

insert into t_company (
	`id`,`name`,`mobile`,`address`,`logo`,`category`,`contacts`,`legal_person`,`legal_card`,`phone`,`email`,`stock_code`,`description`,`create_date`,`update_date`
)
VALUES (1,'希奥信息科技股份有限公司',
'021-5865****','上海市浦东新区春晓路439号','https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2',1,
	'系统管理员','左德昌',null,'13693545297','whl1789015971@163.com','9131000079275685XT','上海希奥信息科技股份有限公司（以下简称：希奥信息 *代码：430632）于2006年9月在上海成立，并于2014年2月成功挂牌新三板',now(),now());

insert into t_user(
	`id`,`user_name`,`nick_name`,`password`,`type`,`status`,`company_id`,`create_date`,`update_date`
) VALUES ('1','admin','上海希奥信息科技股份有限公司','$2a$10$Oi0..EhtLsGK1709cAATceNV2RaQqx5AwpWMslb19mahv9RwtlLIu',1,0,'1',now(),now());

insert into sys_user_role( `user_id`,`role_id`,`create_date`,`update_date`) values('1',1,now(),now());
