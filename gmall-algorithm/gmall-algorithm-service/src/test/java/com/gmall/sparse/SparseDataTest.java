package com.gmall.sparse;

import cn.hutool.core.lang.Assert;
import com.gamll.user.base.BaseJunitTest;
import org.junit.Test;
import org.junit.jupiter.api.Order;

public class SparseDataTest extends BaseJunitTest {

    @Test
    @Order(1)
    public void sparseDataJunitTest(){
        // sparse data init data array
        int[][] sparseArr = new int[11][11];
        // iterator array info
        for (int[] row : sparseArr) {
            for (int data : row) {
                System.out.printf("%d\t",data);
            }
            System.out.println("");
        }
        Assert.isTrue(sparseArr.length == 11);
    }
}
