package com.gmall.sparse;

import lombok.Cleanup;

import java.io.*;

public class SparseDataDemo {
    public static void main(String[] args) throws IOException {
        // sparse data init data array
        int[][] commonArr = new int[11][11];
        // set assign (4,2) (2,6) (3,8)
        commonArr[3][1] = 1;
        commonArr[1][5] = 2;
        commonArr[2][7] = 4;
        // iterator array info
        int validNum = 0;
        validNum = printArray(commonArr, validNum);

        System.out.printf("--------------------数组初始化完成-----------------------");
        System.out.printf("--start to 二维数组  =====》稀疏数组-- \n");

        /*
        *  稀疏数组特性
        *  第一行：原数组总行数 原数组纵列数 总非0数据总数 (sum)
        *  第二行 第一个非0数据所在行 第一个非0数据所在列 具体数值
        *  第二行 第二个非0数据所在行 第二个非0数据所在列 具体数值
        *  第三行 第三个非0数据所在行 第三个非0数据所在列 具体数值
        *  依次类推
        *  稀疏数组
        *  总行数：sum+1
        *  纵列数：3
        * */
        // 定义稀疏数组
        int[][] sparseArr = new int[validNum + 1][3];
        // 定义首行数据
        sparseArr[0][0] = commonArr.length;
        sparseArr[0][1] = commonArr[0].length;
        sparseArr[0][2] = validNum;
        int validPosition = 0;
        for (int i = 0; i<commonArr.length; i++) {
            for (int j = 0; j< commonArr[i].length; j++) {
                if(commonArr[i][j] != 0){
                    validPosition ++;
                    sparseArr[validPosition][0] = i;
                    sparseArr[validPosition][1] = j;
                    sparseArr[validPosition][2] = commonArr[i][j];
                }
            }
        }
        printArray(sparseArr, validNum);
        System.out.printf("--稀疏数组 =======》 还原为原始数据--\n");
        // TODO 1、将稀疏数组数据保存至磁盘文件中
        File file = new File("D:\\data\\sparseData.txt");
        if(!file.exists()){
            file.createNewFile();
        }
        // 写入文件
        writeToFile(sparseArr,file);


        // TODO 2、从磁盘文件读取稀疏数组数据
        int[][] revertArr = new int[sparseArr[0][0]][sparseArr[0][1]];
        for (int i=1; i<sparseArr.length; i++){
            revertArr[sparseArr[i][0]][sparseArr[i][1]] = sparseArr[i][2];
        }

        printArray(revertArr,0);
    }

    public static int printArray(int[][] sparseArr, int validNum){
        for (int[] row : sparseArr) {
            for (int data : row) {
                if(data != 0){
                    validNum ++;
                }
                System.out.printf("%d\t",data);
            }
            System.out.println("");
        }
        return validNum;
    }

    public static void writeToFile(int[][] sparseArr, File file) throws IOException {
        @Cleanup FileWriter fileWriter = new FileWriter(file,true);
        StringBuffer sb = new StringBuffer();
        for (int[] row : sparseArr) {
            for (int data : row) {
                sb.append(data + "\t");
            }
            sb.append("\n");

        }
        fileWriter.write(sb.toString());
    }

    public static int[][] readFromFile(File file) throws IOException {
       @Cleanup FileReader fileReader = new FileReader(file);
       int cnt = 0;
       cnt = fileReader.read();
       return null;
    }
}
