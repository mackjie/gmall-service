import request from '@/utils/request'

export function getResource() {
  return request({
    url: '/vue-element-admin/routes',
    method: 'get'
  })
}

export function getRoles() {
  return request({
    url: '/vue-element-admin/resource',
    method: 'get'
  })
}

export function addRole(data) {
  return request({
    url: '/vue-element-admin/resource',
    method: 'post',
    data
  })
}

export function updateRole(id, data) {
  return request({
    url: `/vue-element-admin/resource/${id}`,
    method: 'put',
    data
  })
}

export function deleteRole(id) {
  return request({
    url: `/vue-element-admin/resource/${id}`,
    method: 'delete'
  })
}
