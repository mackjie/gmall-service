import request from '@/utils/request'

export function login(data) {
  const url = 'http://localhost:9081/user/login'
  const resp = request({
    url: url,
    method: 'post',
    data: data
  })
  return resp
}

export function getInfo() {
  return request({
    url: 'http://localhost:9081/user/userInfo',
    method: 'get',
    data: { }
  })
}

export function userInfo() {
  return request({
    url: 'http://localhost:9081/user/userBaseInfo',
    method: 'get',
    data: { }
  })
}
export function list(query) {
  return request({
    url: 'http://localhost:9081/user/list',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: query
  })
}

export function createUser(data) {
  return request({
    url: 'http://localhost:9081/user/save',
    method: 'post',
    data
  })
}

export function deleteUser(param) {
  return request({
    url: 'http://localhost:9081/user/delete?' + param,
    method: 'get',
    data: undefined
  })
}

export function update(query) {
  return request({
    url: 'http://localhost:9081/user/update',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: query
  })
}

export function logout() {
  return request({
    url: 'http://localhost:9081/user/logout',
    method: 'post'
  })
}
