import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'

const state = {
  token: getToken(),
  name: '',
  avatar: '',
  introduction: '',
  sex: '',
  tagsView: undefined,
  sidebarLogo: undefined,
  roles: []
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_SEX: (state, sex) => {
    state.sex = sex
  },
  SET_TAGS_VIEW: (state, tagsView) => {
    state.tagsView = tagsView
  },
  SET_SIDEBAR_LOGO: (state, headerLogoView) => {
    state.sidebarLogo = headerLogoView
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const x_token = response.headers['authorization']
        commit('SET_TOKEN', x_token)
        resolve()
      }).catch(error => {
        reject(error.toString())
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo().then(response => {
        const { data } = response
        if (!data) {
          reject('Verification failed, please Login again.')
        }
        const role = data.role
        let tagsView_t = true
        let headerLogoView_t = true
        const { username, avatar, address, tagsView, headerLogoView, sex } = data.user

        // roles must be a non-empty array
        if (!role || role.length <= 0) {
          // reject('getInfo: roles must be a non-null array!')
        }
        // 初始化显示操作
        if (tagsView === 1) {
          tagsView_t = false
        }
        if (headerLogoView === 1) {
          headerLogoView_t = false
        }
        commit('SET_ROLES', role)
        commit('SET_NAME', username)
        commit('SET_AVATAR', avatar)
        commit('SET_INTRODUCTION', address)
        commit('SET_SEX', sex)
        commit('SET_TAGS_VIEW', tagsView_t)
        commit('SET_SIDEBAR_LOGO', headerLogoView_t)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        removeToken()
        resetRouter()

        // reset visited views and cached views
        // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
        dispatch('tagsView/delAllViews', null, { root: true })

        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },

  // dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(async resolve => {
      const token = role + '-token'

      commit('SET_TOKEN', token)
      setToken(token)

      const { roles } = await dispatch('getInfo')
      console.log('come int roles is' + roles)
      resetRouter()

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true })

      // dynamically add accessible routes
      router.addRoutes(accessRoutes)

      // reset visited views and cached views
      dispatch('tagsView/delAllViews', null, { root: true })

      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
