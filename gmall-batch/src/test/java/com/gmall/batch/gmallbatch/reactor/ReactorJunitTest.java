package com.gmall.batch.gmallbatch.reactor;

import com.gmall.batch.gmallbatch.GmallBatchApplicationTests;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

/**
 * for: reactor 测试用例
 *
 * @author hl.Wu
 * @date 2020/7/20 15:35
 * gmall-service com.gmall.batch.gmallbatch
 * @since 0.0.1
 */
@Slf4j
public class ReactorJunitTest extends GmallBatchApplicationTests {

    @Test
    @Order(1)
    public void reactorExample1(){
        // 定义流数据集合
        List<String> list = Arrays.asList(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6"
        );
        // 定义reactor 流
        Mono.just(list).subscribe(System.out::println);
        System.out.println();
        Flux.from(Mono.just(list)).switchIfEmpty((Mono)Mono.just("come in")).subscribe(System.out::println);
    }
}
