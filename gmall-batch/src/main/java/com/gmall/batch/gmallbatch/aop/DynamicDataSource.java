package com.gmall.batch.gmallbatch.aop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author HL.Wu
 * @date 2020/6/22 11:32
 * Copyright ©www.sioo.cn Copyright@2009-2020 AII Right Reserve
 */

@Slf4j
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.getDB().toString().toLowerCase();
    }
}