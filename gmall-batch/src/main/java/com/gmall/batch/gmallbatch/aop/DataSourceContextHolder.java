package com.gmall.batch.gmallbatch.aop;

import com.gmall.common.enums.DbEnum;
import lombok.extern.slf4j.Slf4j;

/**
 * @author HL.Wu
 * @date 2020/6/22 11:31
 * Copyright ©www.sioo.cn Copyright@2009-2020 AII Right Reserve
 */
@Slf4j
public class DataSourceContextHolder {

    public static final DbEnum DEFAULT_DB = DbEnum.MASTER;

    public static final ThreadLocal<DbEnum> dbLocal = new ThreadLocal<> ();

    public static void setDB(DbEnum DbEnum){
        dbLocal.set(DbEnum);
    }

    public static DbEnum getDB(){
        if(dbLocal.get() == null){
            return DbEnum.MASTER;
        }
        return dbLocal.get();
    }

    public static void clearDB(){
        dbLocal.remove();
    }
}