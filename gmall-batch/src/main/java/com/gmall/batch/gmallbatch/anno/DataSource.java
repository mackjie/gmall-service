package com.gmall.batch.gmallbatch.anno;

import com.gmall.common.enums.DbEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author HL.Wu
 * @date 2020/6/22 11:29
 * Copyright ©www.sioo.cn Copyright@2009-2020 AII Right Reserve
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {

    DbEnum value() default DbEnum.MASTER;
}
