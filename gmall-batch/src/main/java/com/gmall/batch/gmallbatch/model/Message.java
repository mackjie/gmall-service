package com.gmall.batch.gmallbatch.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/7/6 17:06
 *
 * for: 初始数据类型
 */

@Data
public class Message {

    @CsvBindByPosition(position = 0)
    private String name;

    @CsvBindByPosition(position = 1)
    private String message;

}
