package com.gmall.batch.gmallbatch.mapper;

import com.gmall.batch.gmallbatch.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserMapper {

    int deleteById(Integer id);

    int save(User record);

    User findById(Integer id);

    int update(User record);
}