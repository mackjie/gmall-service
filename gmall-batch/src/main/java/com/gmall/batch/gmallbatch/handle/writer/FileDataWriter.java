package com.gmall.batch.gmallbatch.handle.writer;

import com.gmall.batch.gmallbatch.handle.data.FilePreData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/7/6 17:14
 * gmall-service
 *
 * for: 数据同步保存
 */
@Slf4j
@Component
public class FileDataWriter implements ItemWriter<String> {
    @Autowired
    private FilePreData filePreData;

    /**
     * Process the supplied data element. Will not be called with any null items
     * in normal operation.
     *
     * @param items items to be written
     * @throws Exception if there are errors. The framework will catch the
     *                   exception and convert or rethrow it as appropriate.
     */
    @Override
    public void write(List items) throws Exception {
        List<String> list = items;
        for (String message : list) {
            log.info(" write : {}", message);
        }

    }
}
