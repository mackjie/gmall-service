package com.gmall.batch.gmallbatch.controller;

import com.gmall.batch.gmallbatch.biz.UserBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HL.Wu
 * @date 2020/7/10 14:09
 * gmall-service
 *
 * for:
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserBiz userBiz;

    @RequestMapping("/save")
    public int save(@RequestParam("id") int i) {
        try {
            if (i == 1) {
                return userBiz.saveBase1();
            }else if(i == 2){
                return userBiz.saveBase2();
            }else if(i == 3){
                return userBiz.saveBase1Exception();
            }else {
                return userBiz.saveBase2Exception();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
