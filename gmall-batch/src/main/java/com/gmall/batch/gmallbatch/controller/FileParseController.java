package com.gmall.batch.gmallbatch.controller;

import cn.hutool.core.date.DateUtil;
import com.gmall.batch.gmallbatch.handle.data.FilePreData;
import com.gmall.batch.gmallbatch.handle.read.FileDataReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HL.Wu
 * @date 2020/7/6 17:49
 * gmall-service
 *
 * for: 文件解析请求api
 */
@RestController
@RequestMapping("/task")
public class FileParseController {

    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private Job fileParseJob;
    @Autowired
    private FilePreData filePreData;
    @Autowired
    private FileDataReader fileDataReader;

    @GetMapping("/parseFile")
    public String parseFile(){
        try {
            fileDataReader.setList(filePreData.preFileData());
            JobParameters jobParameters = new JobParametersBuilder().addLong(DateUtil.today(), DateUtil.date().getTime()).toJobParameters();
            // 加入任务调度
            jobLauncher.run(fileParseJob,jobParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  " start to join in jobLauncher and parse file then save to database ";
    }
}
