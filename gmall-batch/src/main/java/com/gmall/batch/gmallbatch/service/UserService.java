package com.gmall.batch.gmallbatch.service;

import com.gmall.batch.gmallbatch.model.User;

/**
 * @author HL.Wu
 * @date 2020/7/10 14:07
 * gmall-service
 * for:
 */
public interface UserService {

    int deleteById(Integer id);

    int save(User record);

    User findById(Integer id);

    int update(User record);
}
