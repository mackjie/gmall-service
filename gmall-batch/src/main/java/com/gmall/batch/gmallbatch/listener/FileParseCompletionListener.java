package com.gmall.batch.gmallbatch.listener;

import com.gmall.batch.gmallbatch.handle.data.FilePreData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author HL.Wu
 * @date 2020/7/6 18:07
 * gmall-service
 *
 * for: 任务完成监听
 */
@Slf4j
@Component
public class FileParseCompletionListener implements JobExecutionListener {
    @Autowired
    private FilePreData filePreData;
    /**
     * Callback before a job executes.
     *
     * @param jobExecution the current {@link JobExecution}
     */
    @Override
    public void beforeJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.STARTING){
            log.info("---------------------------- current file start to parse ---------------------------- ");
        }
    }

    /**
     * Callback after completion of a job. Called after both both successful and
     * failed executions. To perform logic on a particular status, use
     * "if (jobExecution.getStatus() == BatchStatus.X)".
     *
     * @param jobExecution the current {@link JobExecution}
     */
    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED){
            log.info("----------------------------  current file end to parse complete success ---------------------------- ");
            // TODO remove file or update file name
            filePreData.postFileData();
        }
    }
}
