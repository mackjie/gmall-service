package com.gmall.batch.gmallbatch.handle.process;


import com.alibaba.fastjson.JSON;
import com.gmall.batch.gmallbatch.handle.data.FilePreData;
import com.gmall.batch.gmallbatch.model.MessageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/7/6 16:58
 *
 * for: 处理文件内容数据类型转换相关业务逻辑
 */
@Component
@Slf4j
public class FileDataProcess implements ItemProcessor<String,String> {
    @Autowired
    private FilePreData filePreData;

    @Override
    public String process(String s) throws Exception {
        // format data convert data type
        List<MessageModel> list = filePreData.betFileData(s);
        return JSON.toJSONString(list);
    }

}

