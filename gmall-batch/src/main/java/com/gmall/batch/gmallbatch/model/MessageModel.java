package com.gmall.batch.gmallbatch.model;

import lombok.Data;

/**
 * @author HL.Wu
 * @date 2020/7/6 15:40
 *
 * for: 消息类型接收实体
 */
@Data
public class MessageModel {

    private String name;

    private String message;
}
