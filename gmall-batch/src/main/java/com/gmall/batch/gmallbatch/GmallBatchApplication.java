package com.gmall.batch.gmallbatch;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class,MongoDataAutoConfiguration.class, MongoAutoConfiguration.class})
@MapperScan("com.gmall.batch.gmallbatch.mapper")
public class GmallBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(GmallBatchApplication.class, args);
	}

}
