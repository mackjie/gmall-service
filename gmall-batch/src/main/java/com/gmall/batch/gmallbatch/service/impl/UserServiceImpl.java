package com.gmall.batch.gmallbatch.service.impl;

import com.gmall.batch.gmallbatch.mapper.UserMapper;
import com.gmall.batch.gmallbatch.model.User;
import com.gmall.batch.gmallbatch.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author HL.Wu
 * @date 2020/7/10 14:07
 * gmall-service
 *
 * for:
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public int deleteById(Integer id) {
        return userMapper.deleteById(id);
    }

    @Override
    public int save(User record) {
        return userMapper.save(record);
    }

    @Override
    public User findById(Integer id) {
        return userMapper.findById(id);
    }

    @Override
    public int update(User record) {
        return userMapper.update(record);
    }
}
