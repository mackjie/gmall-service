# :cat: 项目介绍
SpringCloud微服务化综合管理后台，包含网关统一调度,服务负载均衡，单点登录权限验证，海量数据批量处理，数据处理流量监控统一管理，监控、日志搜索目前仍在实现中敬请期待！

### 最终愿景
- 做到真正的开箱即用,让您只注重自己的业务开发，不用再考虑环境怎么搭建
- 如果只需要网关不需要其他，只用gmall-gateway 服务即可
- 如果只需要jwt验证，只用gmall-security服务即可，web参考gmall-user请求即可
- 如果只需要海量数据批处理，只用gmall-batch服务即可
- 如果只需要验证码服务，后端只用gmall-captcha服务即可
- 集海量数据高效批处理同时集强大监控于一身的综合后台管理系统。
- 高并发服务(gmall-batch)实现压力水平自动扩容，支持多实例同时部署

# 基本环境搭建
### [安装node](https://nodejs.org/en/)
## 前端
### 开发环境部署
1. 克隆项目
git clone https://gitee.com/mackjie/gmall-service.git

2. 进入项目目录
cd gmall-service/gmall-admin-web

3. 安装依赖
npm install

4. 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

5. 启动服务
npm run dev

## 后端
### 开发环境部署
#### Maven编译项目顺序
1. gmall-common --> gmall-center --> gmall-gateway --> gmall-captcha --> gmall-user --> gmall-security --> gmall-batch

#### Mysql环境搭建
1. 执行 init-create.sql 、init-data.sql文件

2. 安装启动redis环境

3. 修改文件中的mysql、redis链接地址以及相关密码 
gmall-service/ gmall-user / gmall-user-service / src / main / resources / application-dev.yml

#### 项目启动顺序

1. gmall-center、gmall-gateway、gmall-user、gmall-security、gmall-admin-web

##### 服务说明

##### gmall-common
- 基础服务：公用组件服务，文件上传、下载、时间工具、分页工具等

##### gmall-center
- 中心服务：Eureka服务注册中心

##### gmall-gateway
- 网关服务：负责签名算法安全校验、流量控制、异常统一处理,请求转发调用对应服务，负载均衡（轮询、随机）

##### gmall-user
- 用户服务：负责所有用户相关信息、单点登录、注册、个人用户信息相关

##### gmall-security
- 校验服务：负责所有相关校验，用户登录校验，token校验

##### gmall-admin-web
- 前端服务：负责展示所有后台相关页面数据，供用户操作

##### gmall-algorithm
- 算法服务：负责所有相关算法，目前仅用于个人算法练习

##### gmall-captcha
- 验证码服务：负责登录验证码校验

##### gmall-batch
- 批量数据处理服务：负责处理海量数据并将处理后的数据持久化

##### gmall-monitor
- 监控服务：负责服务监控、海量数据流速监控、日志监控、网关Api请求结果监控，Api耗时监控等

## 参考项目
### 前端页面参考开源项目
- https://github.com/PanJiaChen/vue-element-admin/blob/master/README.zh-CN.md
### 验证码参考开源项目
- https://gitee.com/anji-plus/captcha

## 技术栈

### 前端基础技术栈
1. vue
2. node

### 后端基础技术栈
1. springBoot
2. springCloud
3. springCloud gateway
4. springSecurity
5. springBatch
6. RabbitMQ
7. kafka
8. spring Data Elasticsearch
9. jwt单点登录

### 存储技术栈
1. mysql
2. sharding jdbc
3. mongodb
4. redis
5. elk(Elasticsearch + Logstash + Kibana)
6. TiDB（后期存储方向）

### 监控技术栈
1. ELk
2. Prometheus + Grafana
3. K8s

### 部署技术栈
1. rancher
2. docker
3. gitee + Azure


### 未来方向
- 添加海量数据批量处理服务 gmall-batch，支持多实例部署
- 数据存储方式改为TIDB
- 添加日志管理服务 （Kafka + ELK）
- 添加监控 (Prometheus + Grafana + k8s)
- 添加消息服务（gmall-message）,负责海量消息统一管理发送，支持多实例部署
- 使用容器化管理实现压力自动扩容（docker）


## 欢迎您的加入，一切未来可期
- 想学习？ 
- 想实战？
- 想做企业级项目？
- 想跟随未来趋势微服务？
- 想做高并发、高缓存、海量数据处理？
- 想在自己的简历上增加一个自己的特色？

## **骚年，不要犹豫了，加入我们吧！你想要的我们这里都有。We are family**   :fire: 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0703/110229_2dde45a8_2111737.png "屏幕截图.png")

## 补充

### 最后想说
目前也是处于学习阶段，所谓活到老学到老，如果项目对您有所帮助请右上角star，如果有不对的地方，有则改之，无则加勉，切勿附加个人情绪，人无完人嚒！非常感谢！

### 更新说明
#### 2020-07-07
添加批处理服务，实现数据高效批处理

#### 2020-07-02
添加网关服务，实现负载均衡，流量控制，服务统一调度，签名校验都能相关功能

#### 2020-05-27
添加验证码服务，实可以通过拖拽验证码实现登录