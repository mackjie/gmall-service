package com.gmall.security.po;

import com.gmall.user.api.po.User;
import lombok.Data;

import java.util.List;

/**
 * @author HL.Wu
 * @date 2020/5/25 15:38
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class UserPo extends User {

    private List<String> roleList;

}
