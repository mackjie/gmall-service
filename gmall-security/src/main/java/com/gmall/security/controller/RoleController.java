package com.gmall.security.controller;

import com.anji.captcha.util.StringUtils;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.common.util.JwtUtil;
import com.gmall.security.biz.RoleBiz;
import com.gmall.user.api.form.RoleQueryForm;
import com.gmall.user.api.po.Role;
import com.google.common.base.Preconditions;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author HL.Wu
 * @date 2020/6/9 19:01
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@RestController
@Slf4j
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleBiz roleBiz;

    /**
     * 查询用户所拥有的角色信息
     *
     * @return
     */
    @PostMapping("/list")
    public Response<ResultList<Role>> getRoleList(@RequestBody RoleQueryForm roleQueryForm, HttpServletRequest request){
        String token = request.getHeader(JwtUtil.TOKEN_HEADER);
        String companyId = JwtUtil.getCompanyId(token);
        Preconditions.checkArgument(StringUtils.isNotBlank(companyId),"抱歉，只有企业用户才有相应权限");
        roleQueryForm.setCompanyId(companyId);
        return roleBiz.getUserRole(roleQueryForm);
    }

    @PostMapping("/save")
    public Response save(@RequestBody Role role, HttpServletRequest request){
        return roleBiz.save(role);
    }

    @PostMapping("/update")
    public Response update(@RequestBody Role role, HttpServletRequest request){
        return roleBiz.update(role);
    }

    @GetMapping("/delete")
    public Response delete(@RequestParam("id") @NotNull Integer id){
        return roleBiz.deleteById(id);
    }
}
