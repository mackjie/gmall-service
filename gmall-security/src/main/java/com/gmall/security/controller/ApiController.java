package com.gmall.security.controller;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

/**
 * @author HL.Wu
 * @date 2020/6/1 18:10
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@RequestMapping("/api")
@RestController
@Slf4j
public class ApiController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/edcCodePassword")
    public String edcCodePassword(@RequestParam("password") String password){
        return bCryptPasswordEncoder.encode(password);
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        String str = bc.encode("123456");
        boolean sm = bc.matches("123456",str);
        log.info("status:{} \n  str is {}",sm,str);
    }
}
