package com.gmall.security.controller;

import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.security.biz.MenuBiz;
import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author HL.Wu
 * @date 2020/6/9 19:01
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuBiz menuBiz;

    /**
     * 查询用户所拥有的角色信息
     *
     * @return
     */
    @PostMapping("/list")
    public Response<ResultList<Resource>> getRoleList(@RequestBody ResourceQueryForm resourceQueryForm){
        return menuBiz.list(resourceQueryForm);
    }

    @PostMapping("/save")
    public Response save(@RequestBody Resource resource){
        return menuBiz.save(resource);
    }

    @PostMapping("/update")
    public Response update(@RequestBody Resource resource){
        return menuBiz.update(resource);
    }

    @GetMapping("/delete")
    public Response delete(@RequestParam("id") @NotNull Integer id){
        return menuBiz.deleteById(id);
    }

}
