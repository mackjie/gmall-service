package com.gmall.security.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.security.biz.UserBiz;
import com.gmall.common.util.JwtUtil;
import com.gmall.user.api.form.UserQueryForm;
import com.gmall.user.api.po.User;
import com.gmall.user.api.provider.UserClient;
import com.google.common.base.Preconditions;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
*
 * @author HL.Wu
 * @date 2020/5/22 13:36
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserClient userClient;
    @Autowired
    private UserBiz userBiz;

    @GetMapping("/userInfo")
    public Response<JSONObject> info(HttpServletRequest request){
        String userId = JwtUtil.getUserId(request.getHeader(JwtUtil.TOKEN_HEADER));
        return userClient.getAccountInfo(userId);
    }

    @GetMapping("/userBaseInfo")
    public Response<User> userBaseInfo(HttpServletRequest request){
        Response response =  Response.newInstance();
        String userId = JwtUtil.getUserId(request.getHeader(JwtUtil.TOKEN_HEADER));
        response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText(),userClient.findUserById(userId));
        return response;
    }

    @PostMapping("/list")
    public Response<ResultList<User>> list(@RequestBody UserQueryForm userQueryForm){
        return userBiz.list(userQueryForm);
    }

    @PostMapping("/list")
    public Response<ResultList<User>> list(@RequestBody JSONObject jsonObject){
        log.info("come in data is  \n {} ", JSON.toJSONString(jsonObject));
        return Response.newInstance();
        //return userBiz.list(userQueryForm);
    }

    @GetMapping("/delete")
    public Response deleteUser(@RequestParam("id") @NotNull String id){
        Response response = Response.newInstance();
        try {
            int cnt = userClient.delete(id);
            Preconditions.checkArgument(cnt > 0,"抱歉，删除失败");
            response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText());
        }catch (IllegalArgumentException e) {
            response.fail(HttpBizCode.BIZERROR,e.getMessage());
        }catch (Exception e) {
            log.error("{}",e);
            response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        }
        return response;
    }

    @PostMapping("/save")
    public Response save(@RequestBody JSONObject jsonObject){
        Response response = Response.newInstance();
        try {
            int cnt = userClient.save(jsonObject);
            Preconditions.checkArgument(cnt > 0,"抱歉，添加失败");
            response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText());
        }catch (IllegalArgumentException e) {
            response.fail(HttpBizCode.BIZERROR,e.getMessage());
        }catch (Exception e) {
            log.error("{}",e);
            response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        }
        return response;
    }

    @PostMapping("/update")
    public Response updateUser(@RequestBody User user){
        Response response = Response.newInstance();
        try {
            int cnt = userClient.update(user);
            Preconditions.checkArgument(cnt > 0,"抱歉，更新失败");
            response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText());
        }catch (IllegalArgumentException e) {
            response.fail(HttpBizCode.BIZERROR,e.getMessage());
        }catch (Exception e) {
            log.error("{}",e);
            response.fail(HttpBizCode.BIZERROR,HttpBizCode.BIZERROR.getText());
        }
        return response;
    }

    @GetMapping("/task")
    public String getTask(){
        return "This is a timer task";
    }
}
