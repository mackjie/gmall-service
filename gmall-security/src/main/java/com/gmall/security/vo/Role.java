package com.gmall.security.vo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.List;

/**
 *
 * @author HL.Wu
 * @date 2020/5/25 10:29
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
@Table(name = "role")
public class Role extends BasePo{

    /**
     * 角色名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 状态 0：正常；1：禁用；
     */
    @Column(name = "status")
    private Integer status;

    /**
     * 资源列表
     */
    private List<Resource> resources;
}
