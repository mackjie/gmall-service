package com.gmall.security.vo;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author HL.Wu
 * @date 2020/5/25 10:29
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class BasePo implements Serializable {

    /**
     * id 唯一标识
     */
    private String id;

    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 更新时间
     */
    @Column(name = "update_date")
    private Date updateDate;

    /**
     * 创建者
     */
    private String crater;

    /**
     * 更新者
     */
    private String updater;
}
