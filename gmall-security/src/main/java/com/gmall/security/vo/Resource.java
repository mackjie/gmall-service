package com.gmall.security.vo;

import lombok.Data;

/**
 *
 * @author HL.Wu
 * @date 2020/5/25 10:34
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Data
public class Resource extends BasePo{

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 访问url
     */
    private String url;

    /**
     * 图标路径
     */
    private String icon;

    /**
     * 状态 0：启用；1：禁用
     */
    private Integer status;
}
