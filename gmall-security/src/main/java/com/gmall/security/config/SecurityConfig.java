package com.gmall.security.config;

import com.gmall.security.filter.CoreAuthFilter;
import com.gmall.security.filter.CoreAuthenticationEntryPoint;
import com.gmall.security.filter.LoginAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * @author HL.Wu
 * @date 2020/5/25 15:18
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("userDetailServiceImpl")
    private UserDetailsService userDetailsService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        // 添加密码加密方式
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 进行密码加密处理
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 请求url 处理
        http.cors().and().csrf()
                .disable()
                .authorizeRequests()
                // 添加需要校验的url
                //.antMatchers("/gmall/**").authenticated()
                .antMatchers("/**/logout","/api/**","/captcha/**","/static/**").permitAll() // 无需校验
                .antMatchers(HttpMethod.DELETE,"/**/delete").hasAnyRole("ADMIN") // 需要授权的URl
                .anyRequest().authenticated() // need to authenticate
                .and()
                .addFilter(new LoginAuthFilter(authenticationManager()))
                .addFilter(new CoreAuthFilter(authenticationManager()))
                // 默认不需要创建session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // 异常统一处理
                .exceptionHandling().authenticationEntryPoint(new CoreAuthenticationEntryPoint());
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        // 设置资源路径
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
}
