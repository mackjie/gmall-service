package com.gmall.security.biz;

import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.ResourceQueryForm;
import com.gmall.user.api.po.Resource;
import com.gmall.user.api.provider.ResourceClient;
import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author HL.Wu
 * @date 2020/6/9 19:02
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Component
@Slf4j
public class MenuBiz {

    @Autowired
    private ResourceClient resourceClient;

    public Response<ResultList<Resource>> list(ResourceQueryForm resourceQueryForm) {
        return resourceClient.getRoleList(resourceQueryForm);
    }

    public Response save(Resource resource) {
        Response response = Response.newInstance();
        try {
            int cnt = resourceClient.save(resource);
            Preconditions.checkArgument(cnt > 0,"抱歉，添加失败，请刷新页面重新添加");
            response.fill(HttpBizCode.SUCCESS,"添加成功");
        } catch (IllegalArgumentException e) {
            response.fill(HttpBizCode.BIZERROR,e.getMessage());
        } catch (Exception e) {
            response.fill(HttpBizCode.BIZERROR,"抱歉，添加失败，请刷新页面重新添加");
        }
        return response;
    }

    public Response update(Resource resource) {
        Response response = Response.newInstance();
        try {
            int cnt = resourceClient.update(resource);
            Preconditions.checkArgument(cnt > 0,"抱歉，更新失败，请刷新页面重新更新");
            response.fill(HttpBizCode.SUCCESS,"更新成功");
        } catch (IllegalArgumentException e) {
            response.fill(HttpBizCode.BIZERROR,e.getMessage());
        } catch (Exception e) {
            response.fill(HttpBizCode.BIZERROR,"抱歉，更新失败，请刷新页面重新更新");
        }
        return response;
    }

    public Response deleteById(Integer id) {
        Response response = Response.newInstance();
        try {
            int cnt = resourceClient.delete(id);
            Preconditions.checkArgument(cnt > 0,"抱歉，删除失败，请刷新页面重新删除");
            response.fill(HttpBizCode.SUCCESS,"删除成功");
        } catch (IllegalArgumentException e) {
            response.fill(HttpBizCode.BIZERROR,e.getMessage());
        } catch (Exception e) {
            response.fill(HttpBizCode.BIZERROR,"抱歉，删除失败，请刷新页面重新删除");
        }
        return response;
    }
}
