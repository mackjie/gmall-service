package com.gmall.security.biz;

import com.gmall.common.enums.HttpBizCode;
import com.gmall.common.model.Response;
import com.gmall.common.model.ResultList;
import com.gmall.user.api.form.UserQueryForm;
import com.gmall.user.api.po.User;
import com.gmall.user.api.provider.UserClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author HL.Wu
 * @date 2020/6/3 16:51
 * Copyright ©https://blog.csdn.net/qq_31150503 Copyright@2009-2020 AII Right Reserve
 */
@Component
@Slf4j
public class UserBiz {
    @Autowired
    private UserClient userClient;

    /**
     * 查询用户集合列表信息
     *
     * @param userQueryForm
     * @return
     */
    public Response<ResultList<User>> list(UserQueryForm userQueryForm){
        Response response = Response.newInstance();
        ResultList<User> resultList = userClient.list(userQueryForm);
        response.fill(HttpBizCode.SUCCESS,HttpBizCode.SUCCESS.getText(),resultList);
        return response;
    }
}
